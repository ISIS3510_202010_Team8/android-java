package com.example.hk_v1j;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class NegocioPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_negocio_principal);
    }

    public void launchDemografia(View view) {
        Intent intent = new Intent(this, NegDemografia.class);
        startActivity(intent);
    }

    public void launchSistema(View view) {
        Intent intent = new Intent(this, NegSistema.class);
        startActivity(intent);
    }

    public void launchOperativas(View view) {
        Intent intent = new Intent(this, NegOperativas.class);
        startActivity(intent);
    }
}
