package com.example.hk_v1j;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.hk_v1j.databinding.ActivityHomeBinding;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Home extends AppCompatActivity
{
    //Interfaz
    private static TextView tiempopromConsulta;
    private static TextView tiempopromCrear;
    private static TextView textoCritico;

    //Local
    public static DatabaseHelper myDb;
    public static SharedPreferences.Editor editor;



    private DatabaseReference mDatabase;

    public static String fechaConsultas;
    public static int numeroConsultas;
    public static int veces;
    String usuario;
    String contraseña;

    private static ArrayList<Double> tiemposFormConsulta;
    private static ArrayList<Double> tiemposFormCrear;
    private static ArrayList<Double> tiemposFormBuscar;

    public static double tiempoprombuscar;

    public static SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        sharedPreferences=getSharedPreferences("formulario", 0);
        editor=sharedPreferences.edit();
        ActivityHomeBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        setTitle("Menú principal");

        myDb = new DatabaseHelper(this);
        usuario="404";
        contraseña = "404";

        tiempopromConsulta = binding.tiempoprom;
        tiempopromCrear = binding.tiempoprom2;
        textoCritico = binding.pacientescriticos;

        tiemposFormConsulta = new ArrayList<Double>();
        tiemposFormCrear = new ArrayList<Double>();
        tiemposFormBuscar = new ArrayList<Double>();

        tiempoprombuscar = 0;


        Query query = FirebaseDatabase.getInstance().getReference().child("pacientes");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                textoCritico.setText(dataSnapshot.getChildrenCount() + "");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
        query = FirebaseDatabase.getInstance().getReference().child("consultas");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                fechaConsultas=dataSnapshot.child("fecha").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
        numeroConsultas=2;
    }

    @Override
    protected void onRestart(){

        super.onRestart();
    }

    @Override
    protected void onResume(){
        //Clear shared preferences
        //SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("res_mot","");
        editor.putString("res_triage","");
        editor.putString("res_alergias","");
        editor.putString("res_sintomas","");
        editor.putString("res_pulso","");
        editor.putString("res_respiracion","");
        editor.putString("res_temperatura","");
        editor.putString("res_tension","");
        editor.putString("res_area","");
        editor.commit();

        //update fireBase
        if ( isOnline()){
            try{
                String consulta = myDb.getToPush();
                for(String fila: consulta.split(";")){
                    String[] columns=fila.split(":");
                    pushFireBase(columns);
                    myDb.deleteData(columns[0]);
                }
            }catch (Exception e){
            }
        }
        super.onResume();
        Query q= FirebaseDatabase.getInstance().getReference().child("consultas");
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                veces=Integer.parseInt(dataSnapshot.child("veces").getValue().toString());
                mDatabase= FirebaseDatabase.getInstance().getReference("consultas");
                mDatabase.child("veces").setValue(veces+numeroConsultas);
                numeroConsultas=0;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // perform your action here

    }

    public void launchConsulta(View view) {
        Intent intent = new Intent(this, ConsultarCedula.class);
        startActivity(intent);
    }

    public void launchCrear(View view)
    {
        Intent intent = new Intent(this, CheckCamara.class);
        startActivity(intent);
    }

    public void launchListar(View view) {
        if(isOnline()) {
            Intent intent = new Intent(this, Listar.class);
            startActivity(intent);
        }
        else {
            alertarUsuario("Sin conexión", "Ok", "Cancelar", "No fue posible establecer conexión con la base de datos. Intentelo más tarde");
        }
    }

    public void launchPanico(View view) {
        Intent intent = new Intent(this, Panico.class);
        startActivity(intent);
    }

    public void launchNegocio(View view) {
        Intent intent = new Intent(this, NegocioPrincipal.class);
        startActivity(intent);
    }

    public void cerrarSesion(View view)
    {
        if(isOnline()) {
            FirebaseAuth.getInstance().signOut();
            Intent intentmain = new Intent(this, MainActivity.class);
            intentmain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intentmain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intentmain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            MainActivity.logeado = false;
            startActivity(intentmain);
        }
        else
        {
            alertarUsuario("Error de conexión", "Aceptar", "Cancelar", "No fue posible cerrar su sesión porque no hay conexión a internet, por favor verifiquelo e intente nuevamente.");
        }
    }

    public static double recalcularTiemposConsulta(double tiempo)
    {
        tiemposFormConsulta.add(tiempo);

        long suma = 0;
        for (int i = 0; i < tiemposFormConsulta.size(); i++)
        {
            suma += tiemposFormConsulta.get(i);
        }
        long promedio = suma/ tiemposFormConsulta.size();

        DecimalFormat formato = new DecimalFormat("#.00");
        String minutos = formato.format(promedio);

        tiempopromConsulta.setText(minutos + "");
        tiempopromConsulta.setTextSize(14);
        if(promedio <= 3) {
            tiempopromConsulta.setBackgroundResource(R.drawable.anilloverde);
        }
        else {
            tiempopromConsulta.setBackgroundResource(R.drawable.anillorojo);
        }
        return promedio;
    }

    public static double recalcularTiemposCrear(double tiempo)
    {
        tiemposFormCrear.add(tiempo);

        long suma = 0;
        for (int i = 0; i < tiemposFormCrear.size(); i++)
        {
            suma += tiemposFormCrear.get(i);
        }
        long promedio = suma/ tiemposFormCrear.size();

        DecimalFormat formato = new DecimalFormat("#.00");
        String minutos = formato.format(promedio);

        tiempopromCrear.setText(minutos + "");
        tiempopromCrear.setTextSize(14);
        if(promedio <= 3) {
            tiempopromCrear.setBackgroundResource(R.drawable.anilloverde);
        }
        else {
            tiempopromCrear.setBackgroundResource(R.drawable.anillorojo);
        }
        return promedio;
    }

    public static double recalcularTiemposBuscar(double tiempo)
    {
        tiemposFormBuscar.add(tiempo);

        long suma = 0;
        for (int i = 0; i < tiemposFormBuscar.size(); i++)
        {
            suma += tiemposFormBuscar.get(i);
        }
        long promedio = suma/ tiemposFormBuscar.size();

        tiempoprombuscar = promedio;
        return promedio;
    }

    private void pushFireBase(String[] param){
        mDatabase = FirebaseDatabase.getInstance().getReference("pacientes");
        mDatabase.child(param[0]).child("Nombre").setValue(param[1]);
        mDatabase.child(param[0]).child("Fecha").setValue(param[2]);
        mDatabase.child(param[0]).child("Altura").setValue(Double.parseDouble(param[3]));
        mDatabase.child(param[0]).child("Lugar").setValue(param[4]);
        mDatabase.child(param[0]).child("Sexo").setValue(param[5].toUpperCase());
        mDatabase.child(param[0]).child("Telefono").setValue(param[6]);
        mDatabase.child(param[0]).child("Motivo").setValue(param[7]);
        mDatabase.child(param[0]).child("Alergias").setValue(param[8]);
        mDatabase.child(param[0]).child("Sintomas").setValue(param[9]);
        mDatabase.child(param[0]).child("Pulso").setValue(param[10]);
        mDatabase.child(param[0]).child("Respiracion").setValue(param[11]);
        mDatabase.child(param[0]).child("Temperatura").setValue(param[12]);
        mDatabase.child(param[0]).child("Tension").setValue(param[13]);
        mDatabase.child(param[0]).child("Area").setValue(param[15]);
        mDatabase.child(param[0]).child("Triage").setValue(param[14]);
        mDatabase.child(param[0]).child("Sangre").setValue(param[16]);
        mDatabase.child(param[0]).child("Tiempo").setValue(Integer.parseInt(param[19]));
        mDatabase.child(param[0]).child("Hora").setValue(param[17]+":"+param[18]);
        mDatabase.child(param[0]).child("Estado").setValue("1");
        try {
            Query queryVisitas = FirebaseDatabase.getInstance().getReference().child("pacientes").child(param[0]).child("Visitas");
            queryVisitas.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    try{
                        int numero = (int) dataSnapshot.getValue() + 1;
                        mDatabase.child(param[0]).child("Visitas").setValue(numero);

                    }catch (Exception e){
                        //

                        mDatabase.child(param[0]).child("Visitas").setValue(1);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
            numeroConsultas++;
            myDb.deleteData(param[0]);
        }catch (Exception e){
            Log.d("ERR_INS",e.getMessage());
        }
    }

    public void alertarUsuario(String pTitulo, String pPositive, String pNegative, String pMesage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(pTitulo);
        dialog.setMessage(pMesage);
        dialog.setCancelable(true);
        dialog.setPositiveButton(
                pPositive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                    }
                });

        dialog.setNegativeButton(
                pNegative,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });

        AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
    }

    public boolean isOnline() {
        ConnectivityManager cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean wifi = false;
        boolean dataP= false;
        for(Network n:cm.getAllNetworks()){
            NetworkInfo ni=cm.getNetworkInfo(n);
            if(ni.getType()==ConnectivityManager.TYPE_WIFI){
                wifi= ni.isConnected();
            }else if(ni.getType()== ConnectivityManager.TYPE_MOBILE){
                dataP=ni.isConnected();
            }
        }
        return wifi||dataP;
    }

}

