package com.example.hk_v1j;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.Vibrator;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityPanicoBinding;

public class Panico extends AppCompatActivity {

    Vibrator v = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityPanicoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_panico);
        setTitle("Pánico");
        v = (Vibrator) this.getApplicationContext().getSystemService(VIBRATOR_SERVICE);
        long[] pattern = {0, 1000, 100};
        v.vibrate(pattern, 0);
    }
    @Override
    public void onStop(){
        super.onStop();
        v.cancel();
    }
    public void launchHome(View view)
    {
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
        v.cancel();
    }
}
