package com.example.hk_v1j;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityEscanerCrearBinding;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;

public class EscanerCrear extends AppCompatActivity {

    SurfaceView cameraView;
    TextView textView;
    CameraSource cameraSource;
    //SharedPreferences sharedPreferences;
    final int RequestCameraPermissionID = 1001;
    String cedula ="";
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case RequestCameraPermissionID:
            {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        return;
                    }
                    try
                    {
                        cameraSource.start(cameraView.getHolder());
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                }
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //sharedPreferences = getSharedPreferences("formulario", 0);
        ActivityEscanerCrearBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_escaner_crear);

        cameraView = binding.surfaceView;
        textView = binding.escaneando;

        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        if (!textRecognizer.isOperational())
        {
            Log.w("MainActivity", "Detector dependencies are not yet available");

        }
        else
        {
            cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(2.0f)
                    .build();
            cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder)
                {
                    try
                    {
                        while(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                        {
                            ActivityCompat.requestPermissions(EscanerCrear.this, new String[]{Manifest.permission.CAMERA}, RequestCameraPermissionID);
                        }
                        cameraSource.start(cameraView.getHolder());
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                }
                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });

            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {

                }
                String answer="";
                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    int cont=0;
                    int n=items.size();

                    StringBuilder sb=new StringBuilder(n);
                    items.toString();
                    while(cont<n){
                        sb.append(items.valueAt(cont).getValue()+"\n");
                        cont++;
                    }
                    answer=sb.toString();
                    sb=null;
                    System.gc();
                    String valueS=cleanScan(answer);
                    textView.setText(valueS.equals("")?answer:valueS);
                    if(valueS.equals(""))
                        try{textView.setTextSize(6);}catch (Exception e){}
                    else
                        try{textView.setTextSize(10);}catch (Exception e){}
                    try{
                        Thread.sleep(1500);
                    }catch (Exception e){}
                }
            });
        }
    }
    public String cleanScan(String input){
        try{
            String[] cedula1= input.split("\n");

            //salvar 4, 5 y 7
            String[] cedula2 = (cedula1[3].split(" ")[1]).split("\\.");
            String limpia="";
            for(int p = 0; p < cedula2.length; p++)
            {
                limpia += cedula2[p];

            }
            return "Cedula: "+limpia+"\nNombre: "+cedula1[6]+" "+cedula1[4];
        }catch (NullPointerException|ArrayIndexOutOfBoundsException e){

        }
        return "";

    }
    public void launchFormEscaner(View view)
    {
        String msg = textView.getText().toString();
        Intent intent = new Intent(this, EscanerCrearAtras.class);
        //SharedPreferences.Editor editor = sharedPreferences.edit();
        //Log.d("Crear1: ",msg);
        Home.editor.putString("key_frontal",msg);

        Home.editor.commit();
        System.gc();
        startActivity(intent);
    }

    public String getCedula(){
        return cedula;
    }
}
