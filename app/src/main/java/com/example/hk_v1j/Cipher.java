package com.example.hk_v1j;
import android.util.Log;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
class CipherPassword {
    Cipher c;
    SecretKey llave;
    public CipherPassword(){
        try {

            c = Cipher.getInstance("AES_256/ECB/PKCS5Padding");
            llave=new SecretKeySpec("9mav8HKquoreFin2jP3x4Gt5sd6zCl7b".getBytes(), 0,32,"AES");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    protected String cipherText(String planeText){
        try {
            c.init(Cipher.ENCRYPT_MODE,llave);

            return toHexString(c.doFinal(toByteArray(planeText)));
        }catch (Exception e){
            Log.d("OFLOGIN","no pasa el cifrado");
            e.printStackTrace();
            return null;
        }
    }
    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }
    public static byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }
    protected String getText(String cipherText){
        try {
            c.init(Cipher.DECRYPT_MODE, llave);
            return toHexString(c.doFinal(toByteArray(cipherText)));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
