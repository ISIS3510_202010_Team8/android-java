package com.example.hk_v1j;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NegSistema extends AppCompatActivity {

    HorizontalBarChart barTiempoConsulta;
    HorizontalBarChart barTiempoMedia;
    TextView txtConsultas;
    int contador;
    int totalpacientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_negocio_sistema);

        barTiempoConsulta = findViewById(R.id.negociotiempoconsulta);
        barTiempoMedia = findViewById(R.id.negociotiempomedia);
        txtConsultas = findViewById(R.id.negocioconsultas);
        contador = 0;
        totalpacientes = 0;

        setTiempoConsulta(1, 180);
        setTiempoSuperior(1,100);
        setNumeroConsultasMensuales();
    }
    public void setNumeroConsultasMensuales(){
        Query queryConsultas = FirebaseDatabase.getInstance().getReference().child("consultas");
        queryConsultas.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                txtConsultas.setText(dataSnapshot.child("veces").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void setTiempoConsulta(int count, int range)
    {
        ArrayList<BarEntry> yVals = new ArrayList<>();

        Description desc = new Description();
        desc.setText("Prom. tiempo por consulta (seg)");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);
        barTiempoConsulta.setDescription(desc);

        final ArrayList<String> xLabelEdades = new ArrayList<>();
        xLabelEdades.add("Tiempo promedio");

        barTiempoConsulta.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelEdades.get((int) value);
            }
        });

        barTiempoConsulta.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barTiempoConsulta.getAxisRight().setEnabled(false);

        yVals.add(new BarEntry(0, (int) Home.tiempoprombuscar));

        BarDataSet setTiempoConsulta = new BarDataSet(yVals, "Data set Tiempo consulta");
        setTiempoConsulta.setColors(ColorTemplate.JOYFUL_COLORS);

        BarData dataTiempoConsulta = new BarData(setTiempoConsulta);
        dataTiempoConsulta.setValueTextSize(10f);
        dataTiempoConsulta.setValueTextColor(R.color.colorOscuro);

        barTiempoConsulta.setData(dataTiempoConsulta);
    }

    public void setTiempoSuperior(int count, int range)
    {
        ArrayList<BarEntry> yVals = new ArrayList<>();

        Description desc = new Description();
        desc.setText("% de datos que superan el tiempo promedio");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);
        barTiempoMedia.setDescription(desc);

        final ArrayList<String> xLabelPorcentaje = new ArrayList<>();
        xLabelPorcentaje.add("Porcentaje");

        barTiempoMedia.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelPorcentaje.get((int) value);
            }
        });

        barTiempoMedia.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barTiempoMedia.getAxisRight().setEnabled(false);

        Query querytiempos = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querytiempos.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                totalpacientes = (int) dataSnapshot.getChildrenCount();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if(Integer.parseInt(paciente.child("Tiempo").getValue().toString()) >= 5)
                    {
                        contador++;
                    }
                    i++;
                }

                int porcentajesuperiores = contador*100/totalpacientes;

                yVals.add(new BarEntry(0, porcentajesuperiores));

                BarDataSet setTiempoMedia = new BarDataSet(yVals, "Data set superiores a la media");
                setTiempoMedia.setColors(ColorTemplate.JOYFUL_COLORS);

                BarData dataTiempoMedia = new BarData(setTiempoMedia);
                dataTiempoMedia.setValueTextSize(10f);
                dataTiempoMedia.setValueTextColor(R.color.colorOscuro);

                barTiempoMedia.setData(dataTiempoMedia);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

