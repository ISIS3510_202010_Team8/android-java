package com.example.hk_v1j;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityNegocioDemografiaBinding;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NegDemografia extends AppCompatActivity {
    PieChart pieCiudad;
    PieChart PieTriage;
    PieChart pieTiempoSexo, pieEdades;
    BarChart barSexo;
    HorizontalBarChart barTiempoEdad;
    HorizontalBarChart barTriageEdad;

    private int totalpacientes, totalbog, totalotras;
    private int totalh, totalm, totald;
    private int triage1, triage2, triage3;
    private int sumajovenes, sumaadultos, sumavet, sumaviejos;
    private int sumatiemposh, sumatiemposf;
    private int jovenes, adultos, veteranos, viejos;

    List<PieEntry> valorespieCiudades, valorespieTriage, valorespieTiempoSexo, valorespieEdades;
    List<BarEntry> valoresbarSexo;

    ArrayList<Integer> edades;
    ArrayList<Double> tiempos;
    ArrayList<Integer> triages;
    ArrayList<String> sexos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityNegocioDemografiaBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_negocio_demografia);

        pieCiudad = binding.negociopie;
        PieTriage = binding.negociopietriage;
        barSexo = binding.negociobarra;

        barTriageEdad = findViewById(R.id.negociotriageedad);
        pieTiempoSexo = findViewById(R.id.negociotiemposexo);
        pieEdades = findViewById(R.id.negocioedades);
        barTiempoEdad = findViewById(R.id.negociotiempoedad);

        valorespieCiudades = new ArrayList<PieEntry>();
        valorespieTiempoSexo = new ArrayList<PieEntry>();
        valorespieTriage = new ArrayList<PieEntry>();
        valoresbarSexo = new ArrayList<BarEntry>();
        valorespieEdades = new ArrayList<PieEntry>();

        edades = new ArrayList<>();
        tiempos = new ArrayList<>();
        triages = new ArrayList<>();
        sexos = new ArrayList<>();

        totalpacientes = 0;
        totalbog = 0;
        totalotras = 0;

        totalh = 0;
        totalm = 0;
        totald = 0;

        triage1 = 0;
        triage2 = 0;
        triage3 = 0;

        sumajovenes = 0;
        sumaadultos = 0;
        sumavet = 0;
        sumaviejos = 0;

        sumatiemposh = 0;
        sumatiemposf = 0;

        jovenes = 0;
        adultos = 0;
        veteranos = 0;
        viejos = 0;

        setPieCiudad();
        setBarSexo();
        setPieTriage();

        //Barra horizontal
        setData(4, 100);
        setTriageEdad(4, 4);
        setTiempoSexo();
    }

    public void setPieCiudad() {
        Description desc = new Description();
        desc.setText("Bogotá vs. Otras ciudades");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);

        pieCiudad.setDescription(desc);
        pieCiudad.setTransparentCircleRadius(61f);
        pieCiudad.setUsePercentValues(true);

        Query querytotal = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querytotal.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                totalpacientes = (int) dataSnapshot.getChildrenCount();
                Query querybogota = FirebaseDatabase.getInstance().getReference().child("pacientes")
                        .orderByChild("Lugar").equalTo("Bogotá D.C");
                querybogota.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        totalbog = (int) dataSnapshot.getChildrenCount() * 100 / totalpacientes;
                        totalotras = 100 - totalbog;

                        valorespieCiudades.add(new PieEntry(totalbog, "Bogotá"));
                        valorespieCiudades.add(new PieEntry(totalotras, "Otras"));

                        PieDataSet datasetciudades = new PieDataSet(valorespieCiudades, "Ciudades");
                        datasetciudades.setSliceSpace(3f);
                        datasetciudades.setSelectionShift(5f);
                        datasetciudades.setColors(ColorTemplate.JOYFUL_COLORS);

                        PieData dataciudades = new PieData(datasetciudades);
                        dataciudades.setValueTextSize(10f);
                        dataciudades.setValueTextColor(Color.WHITE);
                        pieCiudad.setData(dataciudades);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setBarSexo()
    {
        Description descbar = new Description();
        descbar.setText("Pacientes por sexo");
        descbar.setTextSize(12);
        descbar.setTextColor(R.color.colorOscuro);

        barSexo.setDescription(descbar);
        barSexo.setFitBars(true);
        barSexo.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barSexo.getAxisRight().setEnabled(false);

        final ArrayList<String> xLabelSexo = new ArrayList<>();
        xLabelSexo.add("Hombres");
        xLabelSexo.add("Mujeres");
        xLabelSexo.add("ND");

        barSexo.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelSexo.get((int) value);
            }
        });

        Query queryhombres = FirebaseDatabase.getInstance().getReference().child("pacientes")
                .orderByChild("Sexo").equalTo("M");
        queryhombres.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                totalh = (int) dataSnapshot.getChildrenCount();
                Query querymujeres = FirebaseDatabase.getInstance().getReference().child("pacientes")
                        .orderByChild("Sexo").equalTo("F");
                querymujeres.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        totalm = (int) dataSnapshot.getChildrenCount();
                        totald = totalpacientes - totalm - totalh;

                        valoresbarSexo.add(new BarEntry(0, totalh));
                        valoresbarSexo.add(new BarEntry(1, totalm));
                        valoresbarSexo.add(new BarEntry(2, totald));

                        BarDataSet datasetsexo = new BarDataSet(valoresbarSexo, "Sexo");
                        datasetsexo.setColors(ColorTemplate.JOYFUL_COLORS);
                        datasetsexo.setDrawValues(true);

                        BarData datasexo = new BarData(datasetsexo);
                        datasexo.setBarWidth(0.5f);

                        barSexo.setData(datasexo);
                        barSexo.invalidate();

                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setPieTriage()
    {
        Description descpie = new Description();
        descpie.setText("Pacientes por triage");
        descpie.setTextSize(12);
        descpie.setTextColor(R.color.colorOscuro);

        PieTriage.setDescription(descpie);
        PieTriage.setTransparentCircleRadius(61f);
        PieTriage.setUsePercentValues(true);

        Query querytriage1 = FirebaseDatabase.getInstance().getReference().child("pacientes")
                .orderByChild("Triage").equalTo("1");
        querytriage1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                triage1 = (int) dataSnapshot.getChildrenCount();
                Query querytriage2 = FirebaseDatabase.getInstance().getReference().child("pacientes")
                        .orderByChild("Triage").equalTo("2");
                querytriage2.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        triage2 = (int) dataSnapshot.getChildrenCount();
                        Query querytriage3 = FirebaseDatabase.getInstance().getReference().child("pacientes")
                                .orderByChild("Triage").equalTo("3");
                        querytriage3.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                triage3 = (int) dataSnapshot.getChildrenCount();
                                valorespieTriage.add(new PieEntry(triage1, "Nivel 1"));
                                valorespieTriage.add(new PieEntry(triage2, "Nivel 2"));
                                valorespieTriage.add(new PieEntry(triage3, "Nivel 3"));

                                PieDataSet datasettriage = new PieDataSet(valorespieTriage, "Triage");
                                datasettriage.setSliceSpace(3f);
                                datasettriage.setSelectionShift(5f);
                                datasettriage.setColors(ColorTemplate.COLORFUL_COLORS);

                                PieData datatriage = new PieData(datasettriage);
                                datatriage.setValueTextSize(10f);
                                datatriage.setValueTextColor(Color.WHITE);

                                PieTriage.setData(datatriage);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setData(int count, int range) {
        ArrayList<BarEntry> yVals = new ArrayList<>();

        Description desc = new Description();
        desc.setText("Prom. tiempo por edades");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);
        barTiempoEdad.setDescription(desc);

        final ArrayList<String> xLabelEdades = new ArrayList<>();
        xLabelEdades.add("0-25");
        xLabelEdades.add("25-50");
        xLabelEdades.add("50-75");
        xLabelEdades.add("75-100");

        barTiempoEdad.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelEdades.get((int) value);
            }
        });
        barTiempoEdad.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barTiempoEdad.getAxisRight().setEnabled(false);
        Query querycumpleaños = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querycumpleaños.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    String nac = paciente.child("Fecha").getValue().toString();
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaNac = null;
                    try {
                        fechaNac = formato.parse(nac);

                        int anoNac = fechaNac.getYear() + 1900;
                        int edad = 2020 - anoNac;

                        edades.add(edad);
                        tiempos.add(Double.parseDouble(paciente.child("Tiempo").getValue().toString()));

                    } catch (ParseException ex) {
                        System.out.println(ex);
                    }
                    i++;
                }
                setPieEdades();
                for (int j = 0; j < edades.size(); j++) {
                    if (edades.get(j) >= 0 && edades.get(j) <= 25) {
                        sumajovenes += tiempos.get(j);
                    } else if (edades.get(j) > 25 && edades.get(j) <= 50) {
                        sumaadultos += tiempos.get(j);
                    } else if (edades.get(j) > 50 && edades.get(j) <= 75) {
                        sumavet += tiempos.get(j);
                    } else {
                        sumaviejos += tiempos.get(j);
                    }
                }
                int promediojovenes = sumajovenes / edades.size();
                int promedioadultos = sumaadultos / edades.size();
                int promediovet = sumavet / edades.size();
                int promedioviejos = sumaviejos / edades.size();

                yVals.add(new BarEntry(0, promediojovenes));
                yVals.add(new BarEntry(1, promedioadultos));
                yVals.add(new BarEntry(2, promediovet));
                yVals.add(new BarEntry(3, promedioviejos));

                BarDataSet setTiempoEdad = new BarDataSet(yVals, "Data set Tiempo edad");
                setTiempoEdad.setColors(ColorTemplate.JOYFUL_COLORS);

                BarData dataTiempoEdad = new BarData(setTiempoEdad);
                dataTiempoEdad.setValueTextSize(10f);
                dataTiempoEdad.setValueTextColor(R.color.colorOscuro);

                barTiempoEdad.setData(dataTiempoEdad);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setTriageEdad(int count, int range) {
        ArrayList<BarEntry> yVals = new ArrayList<>();
        Description desc = new Description();
        desc.setText("Prom. triage por edades");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);
        barTriageEdad.setDescription(desc);

        final ArrayList<String> xLabelEdades = new ArrayList<>();
        xLabelEdades.add("0-25");
        xLabelEdades.add("25-50");
        xLabelEdades.add("50-75");
        xLabelEdades.add("75-100");

        sumajovenes = 0;
        sumaadultos = 0;
        sumavet = 0;
        sumaviejos = 0;

        barTriageEdad.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelEdades.get((int) value);
            }
        });
        barTriageEdad.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barTriageEdad.getAxisRight().setEnabled(false);

        Query querytriage = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querytriage.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    triages.add(Integer.parseInt(paciente.child("Triage").getValue().toString()));
                    i++;
                }
                for (int j = 0; j < edades.size(); j++) {
                    if (edades.get(j) >= 0 && edades.get(j) <= 25) {
                        sumajovenes += triages.get(j);
                    } else if (edades.get(j) > 25 && edades.get(j) <= 50) {
                        sumaadultos += triages.get(j);
                    } else if (edades.get(j) > 50 && edades.get(j) <= 75) {
                        sumavet += triages.get(j);
                    } else {
                        sumaviejos += triages.get(j);
                    }
                }
                int promediojovenes = sumajovenes / edades.size();
                int promedioadultos = sumaadultos / edades.size();
                int promediovet = sumavet / edades.size();
                int promedioviejos = sumaviejos / edades.size();

                yVals.add(new BarEntry(0, promediojovenes));
                yVals.add(new BarEntry(1, promedioadultos));
                yVals.add(new BarEntry(2, promediovet));
                yVals.add(new BarEntry(3, promedioviejos));

                BarDataSet setTriageEdad = new BarDataSet(yVals, "Data set Triage edad");
                setTriageEdad.setColors(ColorTemplate.JOYFUL_COLORS);

                BarData dataTriageEdad = new BarData(setTriageEdad);
                dataTriageEdad.setValueTextSize(10f);
                dataTriageEdad.setValueTextColor(R.color.colorOscuro);

                barTriageEdad.setData(dataTriageEdad);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setTiempoSexo()
    {
        Query querytriage = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querytriage.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    sexos.add(paciente.child("Sexo").getValue().toString());
                    i++;
                }
                for (int j = 0; j < sexos.size(); j++) {
                    if (sexos.get(j).equals("M")) {
                        sumatiemposh += tiempos.get(j);
                    } else if (sexos.get(j).equals("F")) {
                        sumatiemposf += tiempos.get(j);
                    }
                }
                int promedioh = sumatiemposh / sexos.size();
                int promediof= sumatiemposf / sexos.size();

                valorespieTiempoSexo.add(new PieEntry(promediof, "Promedio mujeres"));
                valorespieTiempoSexo.add(new PieEntry(promedioh, "Promedio hombres"));

                PieDataSet datasettiemposexo = new PieDataSet(valorespieTiempoSexo, "Tiempo vs. Sexo");
                datasettiemposexo.setSliceSpace(3f);
                datasettiemposexo.setSelectionShift(5f);
                datasettiemposexo.setColors(ColorTemplate.JOYFUL_COLORS);

                PieData datatiemposexo = new PieData(datasettiemposexo);
                datatiemposexo.setValueTextSize(10f);
                datatiemposexo.setValueTextColor(Color.WHITE);
                pieTiempoSexo.setData(datatiemposexo);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void setPieEdades()
    {
        Log.d("Entre", edades.size() + "");
        for(int i = 0; i < edades.size(); i++)
        {
            if(edades.get(i) > 0 && edades.get(i) <= 25)
            {

                jovenes ++;
            }
            else if(edades.get(i) > 25 && edades.get(i) <= 50)
            {
                adultos++;
            }
            else if(edades.get(i) > 50 && edades.get(i) <= 75)
            {
                veteranos++;
            }
            else
            {
                viejos++;
            }
        }

        Query querytotal = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querytotal.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                totalpacientes = (int) dataSnapshot.getChildrenCount();
                int porjovenes = jovenes*100/totalpacientes;
                Log.d("PORCENTAJE JOVENES", jovenes +"");
                Log.d("PORCENTAJE JOVENES", porjovenes +"");
                int poradultos = adultos*100/totalpacientes;
                int porveteranos = veteranos*100/totalpacientes;
                int porviejos = viejos*100/totalpacientes;
                Log.d("PORCENTAJE VIEJOS", viejos +"");
                Log.d("PORCENTAJE VIEJOS", porviejos +"");

                valorespieEdades.add(new PieEntry(porjovenes, "Jovenes"));
                valorespieEdades.add(new PieEntry(poradultos, "Adultos"));
                valorespieEdades.add(new PieEntry(porveteranos, "Veteranos"));
                valorespieEdades.add(new PieEntry(porviejos, "Viejos"));

                PieDataSet datasetedades = new PieDataSet(valorespieEdades, "Edades");
                datasetedades.setSliceSpace(3f);
                datasetedades.setSelectionShift(5f);
                datasetedades.setColors(ColorTemplate.JOYFUL_COLORS);

                PieData dataedades= new PieData(datasetedades);
                dataedades.setValueTextSize(10f);
                dataedades.setValueTextColor(Color.WHITE);
                pieEdades.setData(dataedades);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
