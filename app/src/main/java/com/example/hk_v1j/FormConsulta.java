package com.example.hk_v1j;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hk_v1j.databinding.ActivityFormulariopBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EventListener;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class FormConsulta extends AppCompatActivity {
    EditText cedula, nombre, fecha, lugar, altura, sangre, sexo, telefono;
    EditText motivo, alergias, sintomas, pulso, respiracion, temperatura, tension, area, triage;
    Button btn_enviar, home;

    public static int contador;

    private DatabaseReference mDatabase;
    String limpia = "";
    SharedPreferences sharedPreferences;
    private long start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityFormulariopBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_formulariop);
        start = System.nanoTime();

        contador = 0;

        cedula =  binding.editCedula;
        nombre = binding.editNombre;
        fecha = binding.editFecha;
        altura = binding.editAltura;
        lugar = binding.editLugar;
        sangre = binding.editSangre;
        sexo = binding.editSexo;
        telefono = binding.editTelefono;

        motivo = binding.editMotivo;
        alergias = binding.editAlergias;
        sintomas = binding.editSintomas;
        pulso = binding.editPulso;
        respiracion = binding.editRespiracion;
        temperatura = binding.editTemperatura;
        tension = binding.editTension;
        area = binding.editArea;
        triage = binding.editTriage;

        btn_enviar = binding.btnconsultaenviar;
        home = binding.irhome;
        sharedPreferences = getSharedPreferences("consulta", 0);
        String restored_motivo=sharedPreferences.getString("res_mot",null);
        String restored_triage=sharedPreferences.getString("res_triage",null);
        String restored_alergias=sharedPreferences.getString("res_alergias",null);
        String restored_sintomas=sharedPreferences.getString("res_sintomas",null);
        String restored_pulso=sharedPreferences.getString("res_pulso",null);
        String restored_respiracion=sharedPreferences.getString("res_respiracion",null);
        String restored_temperatura=sharedPreferences.getString("res_temperatura",null);
        String restored_tension=sharedPreferences.getString("res_tension",null);
        String restored_area=sharedPreferences.getString("res_area",null);
        if (!TextUtils.isEmpty(restored_motivo)){
            motivo.setText(restored_motivo);
        }
        if (!TextUtils.isEmpty(restored_triage)){
            triage.setText(restored_triage);
        }
        if (!TextUtils.isEmpty(restored_alergias)){
            alergias.setText(restored_alergias);
        }
        if (!TextUtils.isEmpty(restored_sintomas)){
            sintomas.setText(restored_sintomas);
        }
        if (!TextUtils.isEmpty(restored_pulso)){
            pulso.setText(restored_pulso);
        }
        if (!TextUtils.isEmpty(restored_respiracion)){
            respiracion.setText(restored_respiracion);
        }
        if (!TextUtils.isEmpty(restored_temperatura)){
            temperatura.setText(restored_temperatura);
        }
        if (!TextUtils.isEmpty(restored_tension)){
            tension.setText(restored_tension);
        }
        if (!TextUtils.isEmpty(restored_area)){
            area.setText(restored_area);
        }

        String ced=sharedPreferences.getString("val_cedula",null);
        String cedula1 = ced.split(":")[1].trim();

        if(cedula1 != null) {
            String[] cedula2 = cedula1.split("\\.");

            for (int p = 0; p < cedula2.length; p++) {
                limpia += cedula2[p];
            }

            limpia = limpia.trim();

            mDatabase = FirebaseDatabase.getInstance().getReference().child("pacientes");
            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    cedula.setText(limpia);
                    cedula.setTextSize(12);
                    nombre.setText(dataSnapshot.child(limpia).child("Nombre").getValue().toString());
                    nombre.setTextSize(12);
                    fecha.setText(dataSnapshot.child(limpia).child("Fecha").getValue().toString());
                    fecha.setTextSize(12);
                    altura.setText(dataSnapshot.child(limpia).child("Altura").getValue().toString());
                    altura.setTextSize(12);
                    lugar.setText(dataSnapshot.child(limpia).child("Lugar").getValue().toString());
                    lugar.setTextSize(12);
                    sangre.setText(dataSnapshot.child(limpia).child("Sangre").getValue().toString());
                    sangre.setTextSize(12);
                    sexo.setText(dataSnapshot.child(limpia).child("Sexo").getValue().toString());
                    sexo.setTextSize(12);
                    telefono.setText(dataSnapshot.child(limpia).child("Telefono").getValue().toString());
                    telefono.setTextSize(12);
                    motivo.setText(dataSnapshot.child(limpia).child("Motivo").getValue().toString());
                    motivo.setTextSize(12);
                    sintomas.setText(dataSnapshot.child(limpia).child("Sintomas").getValue().toString());
                    sintomas.setTextSize(12);
                    alergias.setText(dataSnapshot.child(limpia).child("Alergias").getValue().toString());
                    alergias.setTextSize(12);
                    pulso.setText(dataSnapshot.child(limpia).child("Pulso").getValue().toString());
                    pulso.setTextSize(12);
                    respiracion.setText(dataSnapshot.child(limpia).child("Respiracion").getValue().toString());
                    respiracion.setTextSize(12);
                    temperatura.setText(dataSnapshot.child(limpia).child("Temperatura").getValue().toString());
                    temperatura.setTextSize(12);
                    tension.setText(dataSnapshot.child(limpia).child("Tension").getValue().toString());
                    tension.setTextSize(12);
                    area.setText(dataSnapshot.child(limpia).child("Area").getValue().toString());
                    area.setTextSize(12);
                    triage.setText(dataSnapshot.child(limpia).child("Triage").getValue().toString());
                    triage.setTextSize(12);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            Home.numeroConsultas++;
        }
        setTitle("Formulario");
    }
    @Override
    public void onBackPressed(){
        SharedPreferences.Editor editor = getSharedPreferences("consulta", 0).edit();
        editor.putString("res_mot",motivo.getText().toString());
        editor.putString("res_triage",triage.getText().toString());
        editor.putString("res_alergias",alergias.getText().toString());
        editor.putString("res_sintomas",sintomas.getText().toString());
        editor.putString("res_pulso",pulso.getText().toString());
        editor.putString("res_respiracion",respiracion.getText().toString());
        editor.putString("res_temperatura",temperatura.getText().toString());
        editor.putString("res_tension",tension.getText().toString());
        editor.putString("res_area",area.getText().toString());
        editor.commit();

        boolean back=sharedPreferences.getBoolean("estado_cosulta",false);
        if(back){
            Intent intent = new Intent(this, EscanerConsulta.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(this, ConsultarCedula.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        super.onBackPressed();
    }
    public void launchHome(View view) {
        SharedPreferences.Editor editor = getSharedPreferences("consulta", 0).edit();
        editor.putString("res_mot","");
        editor.putString("res_triage","");
        editor.putString("res_alergias","");
        editor.putString("res_sintomas","");
        editor.putString("res_pulso","");
        editor.putString("res_respiracion","");
        editor.putString("res_temperatura","");
        editor.putString("res_tension","");
        editor.putString("res_area","");
        editor.commit();
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    public void enviarFormulario(View view) {

        if(!findViewById(R.id.editCedula).equals("") && !findViewById(R.id.editCedula).equals(" ") && (triage.getText().toString().equals("1") || triage.getText().toString().equals("2") || triage.getText().toString().equals("3"))) {
            String pmotivo = motivo.getText().toString();
            String palergias = alergias.getText().toString();
            String psintomas = sintomas.getText().toString();
            String ppulso = pulso.getText().toString();
            String prespiracion = respiracion.getText().toString();
            String ptemperatura = temperatura.getText().toString();
            String ptension = tension.getText().toString();
            String parea = area.getText().toString();
            String ptriage = triage.getText().toString();


            mDatabase = FirebaseDatabase.getInstance().getReference("pacientes");

            mDatabase.child(limpia).child("Nombre").setValue(nombre.getText().toString());
            mDatabase.child(limpia).child("Fecha").setValue(fecha.getText().toString());
            mDatabase.child(limpia).child("Altura").setValue(altura.getText().toString());
            mDatabase.child(limpia).child("Lugar").setValue(lugar.getText().toString());
            mDatabase.child(limpia).child("Sangre").setValue(sangre.getText().toString());
            mDatabase.child(limpia).child("Sexo").setValue(sexo.getText().toString());
            mDatabase.child(limpia).child("Telefono").setValue(telefono.getText().toString());
            mDatabase.child(limpia).child("Motivo").setValue(pmotivo);
            mDatabase.child(limpia).child("Alergias").setValue(palergias);
            mDatabase.child(limpia).child("Sintomas").setValue(psintomas);
            mDatabase.child(limpia).child("Pulso").setValue(ppulso);
            mDatabase.child(limpia).child("Respiracion").setValue(prespiracion);
            mDatabase.child(limpia).child("Temperatura").setValue(ptemperatura);
            mDatabase.child(limpia).child("Tension").setValue(ptension);
            mDatabase.child(limpia).child("Area").setValue(parea);
            mDatabase.child(limpia).child("Triage").setValue(ptriage);
            mDatabase.child(limpia).child("Estado").setValue("1");




           Calendar now = Calendar.getInstance();
           int horaactual = now.get(Calendar.HOUR_OF_DAY);
           int minactual = now.get(Calendar.MINUTE);
           int diaactual = now.get(Calendar.DAY_OF_MONTH);
           int mesactual = now.get(Calendar.MONTH) + 1;
            Log.d("MES ACTUAL ", mesactual + "");
           int anioactual = now.get(Calendar.YEAR);

           DecimalFormat mFormat= new DecimalFormat("00");
           mFormat.setRoundingMode(RoundingMode.DOWN);
           String Dates =  mFormat.format(Double.valueOf(diaactual)) + "/" +  mFormat.format(Double.valueOf(mesactual)) + "/" +  mFormat.format(Double.valueOf(anioactual));
           String horas = mFormat.format(Double.valueOf(horaactual)) + ":" + mFormat.format(Double.valueOf(minactual));

           String horabd = horas + " " + Dates;
           mDatabase.child(limpia).child("Hora").setValue(horabd);

            Query queryVisitas = FirebaseDatabase.getInstance().getReference().child("pacientes").child(limpia).child("Visitas");
            queryVisitas.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    long numero = (long) dataSnapshot.getValue() + 1;
                    mDatabase.child(limpia).child("Visitas").setValue(numero);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
            Home.numeroConsultas++;
            if(!Home.fechaConsultas.split("/")[1].equals(horas.split(":")[0])){
                DatabaseReference q= FirebaseDatabase.getInstance().getReference("consultas");
                q.child("fecha").setValue(horabd);
            }
            Calendar rightNow = Calendar.getInstance();
            int currentHourIn24Format = rightNow.get(Calendar.HOUR_OF_DAY);

            if (currentHourIn24Format != 24) {
                contador++;
            } else {
                contador = 0;
            }

            if (isOnline()) {
                alertarUsuario("Envio exitoso", "Ir a home", "Cancelar", "El envio del formulario ha sido exitoso.");
            } else {
                alertarUsuario("Sin conexión", "Ir a home", "Cancelar", "No fue posible establecer conexión para enviar el formulario. El formulario se guardará y se enviará automáticamente cuando se recupere la conexión.");
            }

            long elapsedTime = System.nanoTime() - start;
            long min = TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS);

            int minutos = (int) Math.round(min);
            mDatabase.child(limpia).child("Tiempo").setValue(minutos);

            start = 0;
            Home.recalcularTiemposConsulta(min);
        }
    }

    public void getSpeechInput(View view) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if(intent.resolveActivity(getPackageManager()) != null)
        {
            startActivityForResult(intent, 10);
        }
        else {
            Toast.makeText(this, "Este dispositivo no soporta esta funcionalidad", Toast.LENGTH_SHORT).show();
        }
     }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        switch (requestCode) {
            case 10:
                if(resultCode == RESULT_OK && data != null) {
                    ArrayList<String> resultado = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String[] palabras = resultado.get(0).split("\\s+");

                    if(palabras.length >= 2) {
                        if(palabras[0].equals("motivo")) {
                            String motivoVoz = "";
                            int i = 1;
                            while(i < palabras.length) {
                                motivoVoz += palabras[i] + " ";
                                i++;
                            }
                            motivo.setText(motivoVoz);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("alergias")) {
                            String alergiasVoz = "";
                            int i = 1;
                            while(i < palabras.length) {
                                if(i == palabras.length-1)
                                {
                                    alergiasVoz += palabras[i];
                                }
                                else
                                {
                                    alergiasVoz += palabras[i] + ", ";
                                }
                                i++;
                            }
                            alergias.setText(alergiasVoz);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("síntomas")) {
                            String sintomasVoz = "";
                            int i = 1;
                            while(i < palabras.length) {
                                if(i == palabras.length-1) {
                                    sintomasVoz += palabras[i];
                                }
                                else {
                                    sintomasVoz += palabras[i] + ", ";
                                }
                                i++;
                            }
                            sintomas.setText(sintomasVoz);
                            startActivityForResult(intent, 10);
                        }

                        if(palabras[0].equals("pulso")) {
                            pulso.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("temperatura")) {
                            temperatura.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("respiración")) {
                            respiracion.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("tensión")) {
                            tension.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("área")) {
                            area.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("triage")) {
                            triage.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                    }
                    else if(palabras.length == 1 && palabras[0].equals("enviar")) {
                        if(isOnline()) {
                            btn_enviar.performClick();
                        }
                        else {

                        }
                    }
                }
                break;
        }
    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        else {
            return false;
        }
    }

    public void alertarUsuario(String pTitulo, String pPositive, String pNegative, String pMesage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(pTitulo);
        dialog.setMessage(pMesage);
        dialog.setCancelable(true);
        dialog.setPositiveButton(
                pPositive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Button home = findViewById(R.id.irhome);
                        home.performClick();
                    }
                });

        dialog.setNegativeButton(
                pNegative,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });
        AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
    }
}
