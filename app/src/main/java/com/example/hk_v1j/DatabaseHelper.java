package com.example.hk_v1j;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {



    private CipherPassword cipherPassword;


    public static final String DATABASE_NAME = "Cuore.db";
    public static final String TABLE_NAME = "paciente";
    public static final String TABLE_MED= "usuario";
    public static final String CAMP_1="USER";
    public static final String CAMP_2="PASSWORD";
    public static final String COL_1 = "DNI";
    public static final String COL_2 = "NOMBRE";
    public static final String COL_3 = "FECHA";
    public static final String COL_4 = "ALTURA";
    public static final String COL_5 = "LUGAR";
    public static final String COL_6 = "SEXO";
    public static final String COL_7 = "TELEFONO";
    public static final String COL_8 = "MOTIVO";
    public static final String COL_9 = "ALERGIAS";
    public static final String COL_10 = "SINTOMAS";
    public static final String COL_11 = "PULSO";
    public static final String COL_12 = "RESPIRACION";
    public static final String COL_13 = "TEMPERATURA";
    public static final String COL_14 = "TENSION";
    public static final String COL_15 = "AREA";
    public static final String COL_16 = "TRIAGE";
    public static final String COL_18 = "SANGRE";
    public static final String COL_17 = "FLAG";
    public static final String COL_19 = "HORA";
    public static final String COL_20 = "TIEMPO";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        if(cipherPassword==null) cipherPassword=new CipherPassword();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
        db.execSQL("create table " + TABLE_NAME +" (DNI INTEGER PRIMARY KEY,NOMBRE TEXT" +
                ",FECHA TEXT,ALTURA TEXT, LUGAR TEXT, SEXO TEXT, TELEFONO TEXT, " +
                "MOTIVO TEXT, ALERGIAS TEXT, SINTOMAS TEXT, PULSO TEXT, RESPIRACION TEXT, " +
                "TEMPERATURA TEXT, TENSION TEXT, AREA TEXT, TRIAGE TEXT, FLAG TEXT, SANGRE TEXT, HORA TEXT, TIEMPO TEXT)");
        db.execSQL("create table "+ TABLE_MED+" (USER TEXT PRIMARY KEY, PASSWORD TEXT) ");
        }catch(Exception e){
            //ya hay base de datos
            this.getAllData();
        }
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int j){

    }

    public boolean updateFlag( String pFlag, String dni) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,dni);
        contentValues.put(COL_17,pFlag);
        try {
            db.update(TABLE_NAME, contentValues, "DNI = ?", new String[]{dni});
            return true;
        }catch (Exception e){
            return false;
        }

    }
    public String getToPush(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME+ " WHERE FLAG = \"NO\"",null);
        StringBuffer buffer = new StringBuffer();
        if (res.getCount()==0){
            buffer.append("404");
            return buffer.toString();
        }
        while(res.moveToNext()){
            buffer.append(res.getString(0)+":");
            buffer.append(res.getString(1)+":");
            buffer.append(res.getString(2)+":");
            buffer.append(res.getString(3)+":");
            buffer.append(res.getString(4)+":");
            buffer.append(res.getString(5)+":");
            buffer.append(res.getString(6)+":");
            buffer.append(res.getString(7)+":");
            buffer.append(res.getString(8)+":");
            buffer.append(res.getString(9)+":");
            buffer.append(res.getString(10)+":");
            buffer.append(res.getString(11)+":");
            buffer.append(res.getString(12)+":");
            buffer.append(res.getString(13)+":");
            buffer.append(res.getString(14)+":");
            buffer.append(res.getString(15)+":");
            buffer.append(res.getString(17)+":");
            buffer.append(res.getString(18)+":");
            buffer.append(res.getString(19)+";");
        }
        return buffer.toString();
    }
    public boolean insertUser(String user, String pw){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] u=user.split("@");
        contentValues.put(CAMP_1,u[0]);
        contentValues.put(CAMP_2,cipherPassword.cipherText(pw));
        long result= db.insert(TABLE_MED, null, contentValues);
        return result==-1?false:true;
    }
    public void insertData(int dni, String nombre,String fecha,
                              String altura,String lugar,String sexo,
                              String telefono,String motivo,String alergias,
                              String sintomas,String pulso,String respiracion,
                              String temperatura,String tension, String area,
                              String triage, String flag, String sangre, String hora, String tiempo) throws Exception{
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,dni);
        contentValues.put(COL_2,nombre);
        contentValues.put(COL_3,fecha);
        contentValues.put(COL_4,altura);
        contentValues.put(COL_5,lugar);
        contentValues.put(COL_6,sexo);
        contentValues.put(COL_7,telefono);
        contentValues.put(COL_8,motivo);
        contentValues.put(COL_9,alergias);
        contentValues.put(COL_10,sintomas);
        contentValues.put(COL_11,pulso);
        contentValues.put(COL_12,respiracion);
        contentValues.put(COL_13,temperatura);
        contentValues.put(COL_14,tension);
        contentValues.put(COL_15,triage);
        contentValues.put(COL_16,area);
        contentValues.put(COL_17,flag);
        contentValues.put(COL_18,sangre);
        contentValues.put(COL_19, hora);
        contentValues.put(COL_20, tiempo);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            throw new Exception("Error al insertar pareja");
    }
    public String checkUser(String p1, String p2){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select "+CAMP_1+","+CAMP_2+" from "+TABLE_MED+ " WHERE USER = \""+p1.split("@")[0] +"\"",null);
        StringBuffer buffer = new StringBuffer();

        while(res.moveToNext()) {
            buffer.append(res.getString(1));
        }
        return validatePass(buffer.toString(),p2);
    }
    private String validatePass(String password, String p2){
        return cipherPassword.getText(password).equals(p2)?p2:"404";
    }
    public void getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_MED,null);
        StringBuffer buffer = new StringBuffer();
        if (res.getCount()==0){
            buffer.append("404");
            Log.d("DATOS: " ,buffer.toString());
        }
        else{
        while(res.moveToNext()){
            buffer.append("USER :"+res.getString(0));
            Log.d("DATOS: " ,buffer.toString());
        }}
    }


    public Integer deleteData (String dni) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "DNI = ?",new String[] {dni});
    }
}