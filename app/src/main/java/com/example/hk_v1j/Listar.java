package com.example.hk_v1j;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityListarBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Listar extends AppCompatActivity {
    String[] nombres;
    String[] cedulas;
    String[] triages;

    Button btn;

    String cedulaAlta = "";
    private DatabaseReference mDatabase;

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityListarBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_listar);
        setTitle("Pacientes");

        final ListView list = binding.list;

        btn = (Button) findViewById(R.id.btndarAlta);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        Activity actividad = this;

        mDatabase.addValueEventListener(new ValueEventListener() {
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot pacientesnap = dataSnapshot.child("pacientes");
                Iterable<DataSnapshot> pacientesChild = pacientesnap.getChildren();
                long num = pacientesnap.getChildrenCount();

                nombres = new String[(int) num];
                cedulas = new String[(int) num];
                triages = new String[(int) num];

                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    try {
                        String cedulab = paciente.getKey();
                        if(paciente.child("Estado").getValue().toString().equals("1")) {
                            cedulaAlta = cedulab;
                            cedulas[i] = cedulab;
                            Log.d("AHHHHHHhhh", cedulab);
                            nombres[i] = paciente.child("Nombre").getValue().toString();
                            triages[i] = paciente.child("Triage").getValue().toString();
                        }
                        i++;
                    } catch (Exception e) {
                    }
                }
                AdaptadorLista whatever = new AdaptadorLista(actividad, nombres, cedulas, triages);
                listView = binding.list;
                listView.setAdapter(whatever);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });
        Home.numeroConsultas++;
    }

    public void darDeAlta(View view)
    {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("pacientes");
        mDatabase.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                mDatabase.child(cedulaAlta).child("Estado").setValue("0");
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
