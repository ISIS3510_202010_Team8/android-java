package com.example.hk_v1j;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityConsultarCedulaBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;

public class ConsultarCedula extends AppCompatActivity {

    private EditText cedula;
    SharedPreferences sharedPreferences;

    public static long start;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        start = System.nanoTime();
        sharedPreferences = getSharedPreferences("consulta", 0);
        ActivityConsultarCedulaBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_consultar_cedula);
        cedula =  binding.editCedula;
    }

    public void validarCedula(View view)
    {
        String ced = cedula.getText().toString();
        if(isOnline())
        {
            if(!ced.isEmpty()) {
                DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("pacientes");
                db.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (!snapshot.hasChild(ced)) {
                            alertarUsuario("Cédula no encontrada", "Ok", "Cancelar", "La cédula no fue encontrada. Por favor revise el número o cree un nuevo formulario.");
                        }
                        else {
                            long elapsedTime = System.nanoTime() - start;
                            Log.d("TIEMPO DE UNA CONSULTA", elapsedTime + "");
                            long miliseg = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS);
                            start = 0;
                            Home.recalcularTiemposBuscar(miliseg);
                            launchForm(ced);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                Home.numeroConsultas++;
            }
            else
            {
                alertarUsuario("Error", "Ok", "Cancelar", "Por favor ingrese una cédula");
            }
        }
        else
        {
            alertarUsuario("Sin conexión", "Ok", "Cancelar", "No fue posible establecer conexión a la base de datos para consultar la cédula. Por favor cree un nuevo formulario.");
        }
    }

    public void launchEscaner(View view)
    {
        Intent intent = new Intent(this, EscanerConsulta.class);
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putBoolean("estado_cosulta",true);
        editor.commit();
        startActivity(intent);
    }

    public void launchForm(String cedula)
    {
        Intent intent = new Intent(this, FormConsulta.class);
        SharedPreferences.Editor editor = getSharedPreferences("consulta", 0).edit();
        editor.putBoolean("estado_cosulta",false);
        editor.putString("val_cedula","NUMERO:" + cedula);
        editor.commit();
        startActivity(intent);
    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void alertarUsuario(String pTitulo, String pPositive, String pNegative, String pMesage)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(pTitulo);
        dialog.setMessage(pMesage);
        dialog.setCancelable(true);

        dialog.setPositiveButton(
                pPositive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {

                    }
                });

        dialog.setNegativeButton(
                pNegative,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });


        AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
    }

}
