package com.example.hk_v1j;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityFormulariopCrearBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.math.RoundingMode;
import java.security.spec.ECField;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Formulario extends AppCompatActivity
{
    TextView nombre,fecha,lugar,altura,sangre,sexo;

    EditText cedula, motivo, alergias, sintomas, pulso, respiracion, temperatura, tension, area, triage,telefono;
    Intent intent;
    Button btn_enviar, home;
    ImageButton mic;
    //SharedPreferences sharedPreferences;
    private DatabaseReference mDatabase;
    //DatabaseHelper myDb;
    String limpia = "";
    String nombreS="";
    String fechaS="";
    String lugarS="";
    String alturaS="";
    String sangreS="";
    String sexoS="";
    String telefonoS="";
    String pmotivo = "";
    String palergias = "";
    String psintomas = "";
    String ppulso = "";
    String prespiracion = "";
    String ptemperatura = "";
    String ptension = "";
    String parea = "";
    String ptriage = "";
    ActivityFormulariopCrearBinding binding;
    String horabd="";
    int minutos=0;
    //Tiempo
    private long start;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //Home.myDb = new DatabaseHelper(this);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_formulariop_crear);

        start = System.nanoTime();

        cedula = binding.editCedula;
        nombre = binding.editNombre;
        fecha = binding.editFecha;
        altura = binding.editAltura;
        lugar = binding.editLugar;
        sangre = binding.editSangre;
        sexo = binding.editSexo;
        telefono = binding.editTelefono;
        btn_enviar = binding.button3;
        home = binding.irhome;
        motivo = binding.editMotivo;
        triage = binding.editTriage;
        alergias = binding.editAlergias;
        sintomas = binding.editSintomas;
        pulso = binding.editPulso;
        respiracion = binding.editRespiracion;
        temperatura = binding.editTemperatura;
        tension = binding.editTension;
        area = binding.editArea;

        mic=binding.imageButton;

        //sharedPreferences = getSharedPreferences("formulario", 0);
        String restored_motivo=Home.sharedPreferences.getString("res_mot",null);
        String restored_triage=Home.sharedPreferences.getString("res_triage",null);
        String restored_alergias=Home.sharedPreferences.getString("res_alergias",null);
        String restored_sintomas=Home.sharedPreferences.getString("res_sintomas",null);
        String restored_pulso=Home.sharedPreferences.getString("res_pulso",null);
        String restored_respiracion=Home.sharedPreferences.getString("res_respiracion",null);
        String restored_temperatura=Home.sharedPreferences.getString("res_temperatura",null);
        String restored_tension=Home.sharedPreferences.getString("res_tension",null);
        String restored_area=Home.sharedPreferences.getString("res_area",null);
        if (!TextUtils.isEmpty(restored_motivo)){
            motivo.setText(restored_motivo);
        }
        if (!TextUtils.isEmpty(restored_triage)){
            triage.setText(restored_triage);
        }
        if (!TextUtils.isEmpty(restored_alergias)){
            alergias.setText(restored_alergias);
        }
        if (!TextUtils.isEmpty(restored_sintomas)){
            sintomas.setText(restored_sintomas);
        }
        if (!TextUtils.isEmpty(restored_pulso)){
            pulso.setText(restored_pulso);
        }
        if (!TextUtils.isEmpty(restored_respiracion)){
            respiracion.setText(restored_respiracion);
        }
        if (!TextUtils.isEmpty(restored_temperatura)){
            temperatura.setText(restored_temperatura);
        }
        if (!TextUtils.isEmpty(restored_tension)){
            tension.setText(restored_tension);
        }
        if (!TextUtils.isEmpty(restored_area)){
            area.setText(restored_area);
        }
        if (!isOnline()){
            mic.setVisibility(View.INVISIBLE);
        }
        try{
            String msg=Home.sharedPreferences.getString("key_frontal",null);
            Log.d("CREAR: ","BBBB"+msg);
            String[] cedula1= msg.split("\n");

            cedula.setText(cedula1[0].split(" ")[1]);
            nombreS=cedula1[1].split(" ")[1]+" "+cedula1[1].split(" ")[2]+cedula1[1].split(" ")[3]+" "+cedula1[1].split(" ")[4];
            nombre.setText(nombreS);
        }catch (NullPointerException|ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        try{
            String msg=Home.sharedPreferences.getString("key_atras",null);
            Log.d("CREAR: ","AAAA"+msg);
            String[] cedulaBack = msg.split("\n");

            /*
            Estatura 5
            fecha 1
            sangre 7
            sexo 9
            lugar 2-3
            telefono random
             */
            sangreS=cedulaBack[0].split(" ")[1].toUpperCase();
            sangre.setText(sangreS);
            sangre.setTextSize(16);
            alturaS=cedulaBack[1].split(" ")[1].toLowerCase();
            altura.setText(alturaS);
            altura.setTextSize(16);
            fechaS=cedulaBack[2].split(" ")[1].toLowerCase();
            fecha.setText(fechaS);
            fecha.setTextSize(15);
            sexoS=cedulaBack[3].split(" ")[1].toLowerCase();
            sexo.setText(sexoS.toUpperCase());
            sexo.setTextSize(16);
            lugarS=(cedulaBack[4].split(" ")[1]+" "+cedulaBack[4].split(" ")[2]).toLowerCase();
            lugar.setText(lugarS);
            lugar.setTextSize(12);
            telefonoS="";
            telefono.setText(telefonoS);
            telefono.setTextSize(16);
        }catch(Exception e){
            e.printStackTrace();
        }
        if(savedInstanceState!=null){
            this.onRestoreInstanceState(savedInstanceState);

        }
        setTitle("Formulario");
    }

    @Override
    public void onBackPressed(){
        //SharedPreferences.Editor editor = Home.sharedPreferences.edit();
        Home.editor.putString("res_mot",motivo.getText().toString());
        Home.editor.putString("res_triage",triage.getText().toString());
        Home.editor.putString("res_alergias",alergias.getText().toString());
        Home.editor.putString("res_sintomas",sintomas.getText().toString());
        Home.editor.putString("res_pulso",pulso.getText().toString());
        Home.editor.putString("res_respiracion",respiracion.getText().toString());
        Home.editor.putString("res_temperatura",temperatura.getText().toString());
        Home.editor.putString("res_tension",tension.getText().toString());
        Home.editor.putString("res_area",area.getText().toString());
        Home.editor.commit();
        super.onBackPressed();
    }
    @Override
    protected void onStop(){
        super.onStop();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
    @Override
    protected void onResume(){

        super.onResume();
    }


    public void launchHome(View view)
    {
        //SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        Home.editor.putString("res_mot","");
        Home.editor.putString("res_triage","");
        Home.editor.putString("res_alergias","");
        Home.editor.putString("res_sintomas","");
        Home.editor.putString("res_pulso","");
        Home.editor.putString("res_respiracion","");
        Home.editor.putString("res_temperatura","");
        Home.editor.putString("res_tension","");
        Home.editor.putString("res_area","");
        Home.editor.commit();
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }
    public void enviarFormulario(View view) {
        try{
            telefonoS=telefono.getText().toString();
            motivo = binding.editMotivo;
            alergias = binding.editAlergias;
            sintomas = binding.editSintomas;
            pulso = binding.editPulso;
            respiracion = binding.editRespiracion;
            temperatura = binding.editTemperatura;
            tension = binding.editTension;
            area = binding.editArea;
            triage = binding.editTriage;

             pmotivo = motivo.getText().toString();
             palergias = alergias.getText().toString();
             psintomas = sintomas.getText().toString();
             ppulso = pulso.getText().toString();
             prespiracion = respiracion.getText().toString();
             ptemperatura = temperatura.getText().toString();
             ptension = tension.getText().toString();
             parea = area.getText().toString();
             sangreS = sangre.getText().toString();
             ptriage = triage.getText().toString();

            limpia= cedula.getText().toString();

            Calendar now = Calendar.getInstance();
            int horaactual = now.get(Calendar.HOUR_OF_DAY);
            int minactual = now.get(Calendar.MINUTE);
            int diaactual = now.get(Calendar.DAY_OF_MONTH);
            int mesactual = now.get(Calendar.MONTH) + 1;
            int anioactual = now.get(Calendar.YEAR);
            DecimalFormat mFormat= new DecimalFormat("00");
            mFormat.setRoundingMode(RoundingMode.DOWN);
            String Dates =  mFormat.format(Double.valueOf(diaactual)) + "/" +  mFormat.format(Double.valueOf(mesactual)) + "/" +  mFormat.format(Double.valueOf(anioactual));
            String horas = mFormat.format(Double.valueOf(horaactual)) + ":" + mFormat.format(Double.valueOf(minactual));
            horabd = horas + " " + Dates;


            long elapsedTime = System.nanoTime() - start;
            long min = TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS);
            minutos = (int) Math.round(min);


            if(isOnline() && !limpia.equals("") && !limpia.equals(" ") && !findViewById(R.id.editCedula).equals("") && !findViewById(R.id.editCedula).equals(" ")
                    && (ptriage.equals("1")||ptriage.equals("2")||ptriage.equals("3")||ptriage.equals("4"))
            && (!nombreS.equals("") &&!nombreS.equals(" ")))
            {
                mDatabase = FirebaseDatabase.getInstance().getReference("pacientes");
                mDatabase.child(limpia).child("Nombre").setValue(nombreS);
                mDatabase.child(limpia).child("Fecha").setValue(fechaS);
                mDatabase.child(limpia).child("Altura").setValue(""+alturaS);
                mDatabase.child(limpia).child("Lugar").setValue(lugarS);
                mDatabase.child(limpia).child("Sexo").setValue(sexoS);
                mDatabase.child(limpia).child("Telefono").setValue(telefonoS);
                mDatabase.child(limpia).child("Motivo").setValue(pmotivo);
                mDatabase.child(limpia).child("Alergias").setValue(palergias);
                mDatabase.child(limpia).child("Sintomas").setValue(psintomas);
                mDatabase.child(limpia).child("Pulso").setValue(ppulso);
                mDatabase.child(limpia).child("Respiracion").setValue(prespiracion);
                mDatabase.child(limpia).child("Temperatura").setValue(ptemperatura);
                mDatabase.child(limpia).child("Tension").setValue(ptension);
                mDatabase.child(limpia).child("Area").setValue((parea.equals("General")||parea.equals("Dermatología")||parea.equals("Obstetricia")||parea.equals("Ortopedia")||parea.equals("Pediatría")||parea.equals("Psiquiatría"))?parea:"General");
                mDatabase.child(limpia).child("Triage").setValue(ptriage);
                mDatabase.child(limpia).child("Sangre").setValue(sangreS);

                mDatabase.child(limpia).child("Hora").setValue(horabd);

                mDatabase.child(limpia).child("Tiempo").setValue(minutos);
                mDatabase.child(limpia).child("Estado").setValue("1");
                start = 0;
                Home.recalcularTiemposCrear(min);
                Query queryVisitas = FirebaseDatabase.getInstance().getReference().child("pacientes").child(limpia).child("Visitas");
                queryVisitas.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        int numero;
                        try{
                            numero= (int) dataSnapshot.getValue() + 1;
                        }catch (Exception e){
                            numero=1;
                        }
                        mDatabase.child(limpia).child("Visitas").setValue(numero);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
                Home.numeroConsultas++;

                if(!Home.fechaConsultas.split("/")[1].equals(horas.split(":")[0])){
                    DatabaseReference q= FirebaseDatabase.getInstance().getReference("consultas");
                    q.child("fecha").setValue(horabd);
                }
                alertarUsuario("Envio exitoso", "Ir a home", "Cancelar", "El envio del formulario ha sido exitoso.");


            }
            else if(!isOnline())
            {
                AlertDialog.Builder dialog = new AlertDialog.Builder(Formulario.this);
                dialog.setTitle("Sin conexión");
                dialog.setMessage("No fue posible establecer conexión para enviar el formulario. El formulario se guardará localmente \n y se enviará automáticamente cuando se recupere la conexión.");
                dialog.setCancelable(true);

                dialog.setPositiveButton(
                        "Ir a Home",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                //SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
                                Home.editor.putString("res_mot","");
                                Home.editor.putString("res_triage","");
                                Home.editor.putString("res_alergias","");
                                Home.editor.putString("res_sintomas","");
                                Home.editor.putString("res_pulso","");
                                Home.editor.putString("res_respiracion","");
                                Home.editor.putString("res_temperatura","");
                                Home.editor.putString("res_tension","");
                                Home.editor.putString("res_area","");
                                Home.editor.putString("res_sangre","");
                                Home.editor.commit();
                                home.performClick();
                            }
                        });

                dialog.setNegativeButton(
                        "Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });


                AlertDialog alert = dialog.create();
                alert.show();
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
                guardarLocal();
              throw new Exception();
            }
        }catch(Exception e){
            //guardar local

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        switch (requestCode)
        {
            case 10:
                if(resultCode == RESULT_OK && data != null&& isOnline())
                {
                    ArrayList<String> resultado = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String[] palabras = resultado.get(0).split("\\s+");

                    if(palabras.length >= 2)
                    {
                        if(palabras[0].equals("motivo"))
                        {
                            String motivoVoz = "";
                            int i = 1;
                            while(i < palabras.length)
                            {
                                motivoVoz += palabras[i] + " ";
                                i++;
                            }
                            motivo.setText(motivoVoz);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("alergias"))
                        {
                            String alergiasVoz = "";
                            int i = 1;
                            while(i < palabras.length)
                            {
                                if(i == palabras.length-1)
                                {
                                    alergiasVoz += palabras[i];
                                }
                                else
                                {
                                    alergiasVoz += palabras[i] + ", ";
                                }
                                i++;
                            }
                            alergias.setText(alergiasVoz);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("síntomas"))
                        {
                            String sintomasVoz = "";
                            int i = 1;
                            while(i < palabras.length)
                            {
                                if(i == palabras.length-1)
                                {
                                    sintomasVoz += palabras[i];
                                }
                                else
                                {
                                    sintomasVoz += palabras[i] + ", ";
                                }
                                i++;
                            }
                            sintomas.setText(sintomasVoz);
                            startActivityForResult(intent, 10);
                        }

                        if(palabras[0].equals("pulso"))
                        {
                            pulso.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("temperatura"))
                        {
                            temperatura.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("respiración"))
                        {
                            respiracion.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("tensión"))
                        {
                            tension.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("área"))
                        {
                            area.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                        if(palabras[0].equals("triage"))
                        {
                            triage.setText(palabras[1]);
                            startActivityForResult(intent, 10);
                        }
                    }
                    else if(palabras.length == 1 && palabras[0].equals("enviar"))
                    {
                        if(isOnline())
                        {
                            btn_enviar.performClick();
                        }
                        else
                        {

                        }
                    }
                }
                break;
        }
    }
    public void alertarUsuario(String pTitulo, String pPositive, String pNegative, String pMesage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(pTitulo);
        dialog.setMessage(pMesage);
        dialog.setCancelable(true);
        dialog.setPositiveButton(
                pPositive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Button home = binding.irhome;
                        home.performClick();
                    }
                });

        dialog.setNegativeButton(
                pNegative,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });
        AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
    }
    public boolean isOnline()
    {
        ConnectivityManager cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean wifi = false;
        boolean dataP= false;
        for(Network n:cm.getAllNetworks()){
            NetworkInfo ni=cm.getNetworkInfo(n);
            if(ni.getType()==ConnectivityManager.TYPE_WIFI){
                wifi= ni.isConnected();
            }else if(ni.getType()== ConnectivityManager.TYPE_MOBILE){
                dataP=ni.isConnected();
            }
        }
        boolean conected = wifi||dataP;
        if(!conected){

            Toast.makeText(Formulario.this,"No se puede acceder a internet,\n verifique su conexión",Toast.LENGTH_LONG).show();

        }

        return conected;
    }
    protected void guardarLocal(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                      try{
                          Home.myDb.insertData(Integer.parseInt(cedula.getText().toString()),nombreS,fechaS,alturaS,lugarS,sexoS,
                                  telefonoS,pmotivo,palergias,psintomas,ppulso,prespiracion,ptemperatura,ptension,parea,ptriage,"NO", sangreS,horabd,""+minutos);
                      }catch (Exception e){
                          e.printStackTrace();
                      }

            }
        }).start();

    }
    public void getSpeechInput(View view)
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if(intent.resolveActivity(getPackageManager()) != null)
        {
            startActivityForResult(intent, 10);
        }
        else
        {
            Toast.makeText(this, "Este dispositivo no soporta esta funcionalidad", Toast.LENGTH_SHORT).show();
        }

    }
}
