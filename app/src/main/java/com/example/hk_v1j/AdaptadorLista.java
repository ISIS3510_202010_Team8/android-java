package com.example.hk_v1j;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdaptadorLista extends ArrayAdapter{

    private final Activity context;

    private final String[] Nombresarray;

    private final String[] CedulasArray;

    private final String[] TriagesArray;

    public AdaptadorLista(Activity pcontext, String[] nombresArrayParam, String[] cedulasArrayParam, String[] triagesArrayParam)
    {
        super(pcontext, R.layout.row_layout , nombresArrayParam);

        this.context=pcontext;
        this.Nombresarray = nombresArrayParam;
        this.CedulasArray= cedulasArrayParam;
        this.TriagesArray = triagesArrayParam;
    }

    public View getView(int position, View view, ViewGroup parent)
    {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.row_layout, null,true);

        EditText nombreEdit= rowView.findViewById(R.id.editNombreLista);
        EditText cedulaEdit = rowView.findViewById(R.id.editCedulaLista);
        EditText triageEdit = rowView.findViewById(R.id.editTriageLista);

        if(Nombresarray[position] != null) {
            nombreEdit.setText(Nombresarray[position]);
            nombreEdit.setTextSize(16);
        }
        else{
            rowView.findViewById(R.id.nombreLista).setVisibility(View.GONE);
            nombreEdit.setVisibility(View.GONE);
        }

        if(CedulasArray[position] != null) {
            cedulaEdit.setText(CedulasArray[position]);
            cedulaEdit.setTextSize(16);
        }
        else {
            rowView.findViewById(R.id.cedulaLista).setVisibility(View.GONE);
            cedulaEdit.setVisibility(View.GONE);
        }

        if(TriagesArray[position] != null) {
            triageEdit.setText(TriagesArray[position]);
            triageEdit.setTextSize(16);
        }
        else{
            rowView.findViewById(R.id.triageLista).setVisibility(View.GONE);
            triageEdit.setVisibility(View.GONE);
            rowView.findViewById(R.id.btndarAlta).setVisibility(View.GONE);
        }

        return rowView;
    };



}
