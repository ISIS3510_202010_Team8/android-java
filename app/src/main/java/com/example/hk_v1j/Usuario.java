package com.example.hk_v1j;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityUsuarioBinding;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Usuario extends AppCompatActivity
{
    private ImageView user;
    private TextView saludo;
    private TextView numPaciente, numvisitas;

    EditText motivo, alergias, sintomas, pulso, respiracion, temperatura, tension, area, triage;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ActivityUsuarioBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_usuario);
        setTitle("Bienvenido/a");

        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        user = binding.imageGirl;
        saludo = binding.textoHola;
        numPaciente = binding.numeropaciente;
        numvisitas = binding.cajavisitas;

        motivo = binding.editMotivou;
        alergias = binding.editAlergiasu;
        sintomas = binding.editSintomasu;
        pulso = binding.editPulsou;
        respiracion = binding.editRespiracionu;
        temperatura = binding.editTemperaturau;
        tension = binding.editTensionu;
        area = binding.editAreau;
        triage = binding.editTriageu;

        String cedula = getSharedPreferences("usuario", 0).getString("key_cedula", "");

        Query querypersona = FirebaseDatabase.getInstance().getReference().child("pacientes").child(cedula).child("Sexo");
        try{Home.numeroConsultas++;}catch (Exception e){}
        querypersona.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue().equals("M"))
                {
                    user.setImageResource(R.drawable.menuser);

                }
                Query querynombre = FirebaseDatabase.getInstance().getReference().child("pacientes").child(cedula).child("Nombre");
                querynombre.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        saludo.setText("¡Hola, " + dataSnapshot.getValue() + "!" );

                        Query queryestado = FirebaseDatabase.getInstance().getReference().child("pacientes").child(cedula);
                        queryestado.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                motivo.setText(dataSnapshot.child("Motivo").getValue().toString());
                                motivo.setTextSize(14);

                                alergias.setText(dataSnapshot.child("Alergias").getValue().toString());
                                alergias.setTextSize(14);

                                sintomas.setText(dataSnapshot.child("Sintomas").getValue().toString());
                                sintomas.setTextSize(14);

                                pulso.setText(dataSnapshot.child("Pulso").getValue().toString());
                                pulso.setTextSize(14);

                                respiracion.setText(dataSnapshot.child("Respiracion").getValue().toString());
                                respiracion.setTextSize(14);

                                temperatura.setText(dataSnapshot.child("Temperatura").getValue().toString());
                                temperatura.setTextSize(14);

                                tension.setText(dataSnapshot.child("Tension").getValue().toString());
                                tension.setTextSize(14);

                                area.setText(dataSnapshot.child("Area").getValue().toString());
                                area.setTextSize(14);

                                triage.setText(dataSnapshot.child("Triage").getValue().toString());
                                triage.setTextSize(14);

                                numvisitas.setText("¡Nunca olvides cuidar tu salud! Has visitado este centro hospitalario " + dataSnapshot.child("Visitas").getValue().toString() + " veces");
                                numvisitas.setTextColor(getResources().getColor(R.color.colorOscuro));
                                numvisitas.setTextSize(16);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        numPaciente.setText("Aguarda por favor, eres nuestro paciente número " + FormConsulta.contador + " del día");
        numPaciente.setTextSize(16);
        numPaciente.setTextColor(getResources().getColor(R.color.colorOscuro));



    }

    public void cerrarSesionUser(View view){
        if (isOnline()) {
            FirebaseAuth.getInstance().signOut();
            Intent intentmain = new Intent(this, MainActivity.class);
            intentmain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intentmain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intentmain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            MainActivity.logeado = false;
            startActivity(intentmain);

        } else {
            alertarUsuario("Error de conexión", "Aceptar", "Cancelar", "No fue posible cerrar su sesión porque no hay conexión a internet, por favor verifiquelo e intente nuevamente.");
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean wifi = false;
        boolean dataP= false;
        for(Network n:cm.getAllNetworks()){
            NetworkInfo ni=cm.getNetworkInfo(n);
            if(ni.getType()==ConnectivityManager.TYPE_WIFI){
                wifi= ni.isConnected();
            }else if(ni.getType()== ConnectivityManager.TYPE_MOBILE){
                dataP=ni.isConnected();
            }
        }
        return wifi||dataP;
    }

    public void alertarUsuario(String pTitulo, String pPositive, String pNegative, String pMesage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(pTitulo);
        dialog.setMessage(pMesage);
        dialog.setCancelable(true);
        dialog.setPositiveButton(
                pPositive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                    }
                });

        dialog.setNegativeButton(
                pNegative,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });

        AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
    }
}
