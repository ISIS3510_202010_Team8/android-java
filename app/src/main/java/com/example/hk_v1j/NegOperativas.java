package com.example.hk_v1j;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class NegOperativas extends AppCompatActivity {

    BarChart barSexoSemana;
    HorizontalBarChart barEspecialidad, barTiempoTriage;
    PieChart pieFranja;
    BarChart barSexoTriage;

    List<BarEntry> valoresbarSexoSemana, valoresbarSexoTriage;
    List<PieEntry> valorespieFranja;

    int hlunes, hmartes, hmiercoles, hjueves, hviernes, hfds;
    int mlunes, mmartes, mmiercoles, mjueves, mviernes, mfds;

    int madrugada, mañana, tarde, noche;

    int general, derma, orto, pediatria, psiquatria, obste;

    TextView proporcion, sangre;

    int entrada, salida;

    int triageunoh, triagedosh, triagetresh, triageunom, triagedosm, triagetresm;

    int amas, bmas, omas, abmas,amenos,bmenos, omenos, abmenos;

    int sumaniveluno, sumaniveldos, sumaniveltres, totalpacientes;

    int conuno, condos, contres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_negocio_operativas);

        barSexoSemana = findViewById(R.id.negociosexosemana);
        pieFranja = findViewById(R.id.negociofranja);
        barEspecialidad = findViewById(R.id.negocioespecialidad);
        proporcion = findViewById(R.id.negocioproporcion);
        sangre = findViewById(R.id.negociosangre);
        barSexoTriage = findViewById(R.id.negocioSexoTriage);
        barTiempoTriage =findViewById(R.id.negociotiempotriage);

        valoresbarSexoSemana = new ArrayList<>();
        valorespieFranja = new ArrayList<>();
        valoresbarSexoTriage = new ArrayList<>();

        hlunes = 0;
        hmartes = 0;
        hmiercoles = 0;
        hjueves = 0;
        hviernes = 0;
        hfds = 0;
        mlunes = 0;
        mmartes = 0;
        mmiercoles = 0;
        mjueves = 0;
        mviernes = 0;
        mfds = 0;

        madrugada = 0;
        mañana = 0;
        tarde = 0;
        noche = 0;

        general = 0;
        obste = 0;
        pediatria = 0;
        psiquatria = 0;
        derma = 0;
        orto = 0;

        entrada = 0;
        salida = 0;

        triageunoh = 0;
        triagedosh = 0;
        triagetresh = 0;
        triageunom = 0;
        triagedosm = 0;
        triagetresm = 0;

        amas = 0;
        bmas = 0;
        omas = 0;
        abmas = 0;
        amenos = 0;
        bmenos=0;
        omenos=0;
        abmenos=0;

        sumaniveldos = 0;
        sumaniveluno = 0;
        sumaniveltres = 0;
        totalpacientes = 0;

        conuno =0;
        condos=0;
        contres=0;

        setBarSexoSemana();
        setPieFranja();
        setTiempoConsulta(6, 100);
        setProporcion();
        setSexoTriage();
        setSangre();
        setTiempoTriage(3, 10);
    }

    public void setBarSexoSemana() {
        Description descbar = new Description();
        descbar.setText("Sexo por día de la semana");
        descbar.setTextSize(12);
        descbar.setTextColor(R.color.colorOscuro);

        barSexoSemana.setDescription(descbar);
        barSexoSemana.setFitBars(true);
        barSexoSemana.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barSexoSemana.getAxisRight().setEnabled(false);

        final ArrayList<String> xLabelSexo = new ArrayList<>();
        xLabelSexo.add("Lunes");
        xLabelSexo.add("Lunes");
        xLabelSexo.add("Martes");
        xLabelSexo.add("Martes");
        xLabelSexo.add("Miercoles");
        xLabelSexo.add("Miercoles");
        xLabelSexo.add("Jueves");
        xLabelSexo.add("Jueves");
        xLabelSexo.add("Viernes");
        xLabelSexo.add("Viernes");
        xLabelSexo.add("FDS");
        xLabelSexo.add("FDS");

        barSexoSemana.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelSexo.get((int) value);
            }
        });

        Query querydiashombres = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querydiashombres.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if (paciente.child("Sexo").getValue().toString().equals("M")) {
                        if (!paciente.child("Hora").getValue().toString().isEmpty()) {
                            String[] fecha = new String[2];
                            fecha = paciente.child("Hora").getValue().toString().split("\\s+");

                            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                            Date fechadate = null;
                            try {
                                fechadate = formato.parse(fecha[1].toString());
                                Calendar c = Calendar.getInstance();
                                c.setTime(fechadate);
                                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

                                if (dayOfWeek == 1 || dayOfWeek == 7) {
                                    hfds++;
                                } else if (dayOfWeek == 2) {
                                    hlunes++;
                                } else if (dayOfWeek == 3) {
                                    hmartes++;
                                } else if (dayOfWeek == 4) {
                                    hmiercoles++;
                                } else if (dayOfWeek == 5) {
                                    hjueves++;
                                } else if (dayOfWeek == 6) {
                                    hviernes++;
                                }
                            } catch (Exception e) {

                            }

                        }
                    }
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Query querydiasmujeres = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querydiasmujeres.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if (paciente.child("Sexo").getValue().toString().equals("F")) {
                        if (!paciente.child("Hora").getValue().toString().isEmpty()) {
                            String[] fecha = paciente.child("Hora").getValue().toString().split("\\s+");

                            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                            Date fechadate = null;
                            try {
                                fechadate = formato.parse(fecha[1].toString());
                                Calendar c = Calendar.getInstance();
                                c.setTime(fechadate);
                                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

                                if (dayOfWeek == 1 || dayOfWeek == 7) {
                                    mfds++;
                                } else if (dayOfWeek == 2) {
                                    mlunes++;
                                } else if (dayOfWeek == 3) {
                                    mmartes++;
                                } else if (dayOfWeek == 4) {
                                    mmiercoles++;
                                } else if (dayOfWeek == 5) {
                                    mjueves++;
                                } else if (dayOfWeek == 6) {
                                    mviernes++;
                                }
                            } catch (Exception e) {

                            }

                        }
                    }
                }

                valoresbarSexoSemana.add(new BarEntry(0, hlunes));
                valoresbarSexoSemana.add(new BarEntry(1, mlunes));
                valoresbarSexoSemana.add(new BarEntry(2, hmartes));
                valoresbarSexoSemana.add(new BarEntry(3, mmartes));
                valoresbarSexoSemana.add(new BarEntry(4, hmiercoles));
                valoresbarSexoSemana.add(new BarEntry(5, mmiercoles));
                valoresbarSexoSemana.add(new BarEntry(6, hjueves));
                valoresbarSexoSemana.add(new BarEntry(7, mjueves));
                valoresbarSexoSemana.add(new BarEntry(8, hviernes));
                valoresbarSexoSemana.add(new BarEntry(9, mviernes));
                valoresbarSexoSemana.add(new BarEntry(10, hfds));
                valoresbarSexoSemana.add(new BarEntry(11, mfds));

                BarDataSet datasetsexo = new BarDataSet(valoresbarSexoSemana, "Sexo");
                datasetsexo.setColors(ColorTemplate.JOYFUL_COLORS);
                datasetsexo.setDrawValues(true);

                BarData datasexo = new BarData(datasetsexo);
                datasexo.setBarWidth(0.5f);

                barSexoSemana.setData(datasexo);
                barSexoSemana.invalidate();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void setPieFranja() {
        Description desc = new Description();
        desc.setText("Franja horario");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);

        pieFranja.setDescription(desc);
        pieFranja.setTransparentCircleRadius(61f);
        pieFranja.setUsePercentValues(true);

        Query queryfranja = FirebaseDatabase.getInstance().getReference().child("pacientes");
        queryfranja.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if (!paciente.child("Hora").getValue().toString().isEmpty()) {
                        String[] fecha = paciente.child("Hora").getValue().toString().split(":");
                        if (Integer.parseInt(fecha[0]) >= 0 && Integer.parseInt(fecha[0]) <= 6) {
                            madrugada++;
                        } else if (Integer.parseInt(fecha[0]) > 6 && Integer.parseInt(fecha[0]) <= 12) {
                            mañana++;
                        } else if (Integer.parseInt(fecha[0]) > 12 && Integer.parseInt(fecha[0]) <= 18) {
                            tarde++;
                        } else {
                            noche++;
                        }
                    }
                }
                valorespieFranja.add(new PieEntry(madrugada, "Madrugada"));
                valorespieFranja.add(new PieEntry(mañana, "Mañana"));
                valorespieFranja.add(new PieEntry(tarde, "Tarde"));
                valorespieFranja.add(new PieEntry(noche, "Noche"));

                PieDataSet datasetfranja = new PieDataSet(valorespieFranja, "Franjas");
                datasetfranja.setSliceSpace(3f);
                datasetfranja.setSelectionShift(5f);
                datasetfranja.setColors(ColorTemplate.JOYFUL_COLORS);

                PieData dataFranjas = new PieData(datasetfranja);
                dataFranjas.setValueTextSize(10f);
                dataFranjas.setValueTextColor(Color.WHITE);
                pieFranja.setData(dataFranjas);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setTiempoConsulta(int count, int range) {
        ArrayList<BarEntry> yVals = new ArrayList<>();

        Description desc = new Description();
        desc.setText("Carga especialidad");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);
        barEspecialidad.setDescription(desc);

        final ArrayList<String> xLabelEdades = new ArrayList<>();
        xLabelEdades.add("General");
        xLabelEdades.add("Dermatología");
        xLabelEdades.add("Obstetricia");
        xLabelEdades.add("Ortopedia");
        xLabelEdades.add("Pediatria");
        xLabelEdades.add("Psiquatria");

        barEspecialidad.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelEdades.get((int) value);
            }
        });

        barEspecialidad.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barEspecialidad.getAxisRight().setEnabled(false);

        Query queryespecialidad = FirebaseDatabase.getInstance().getReference().child("pacientes");
        queryespecialidad.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int total = (int) dataSnapshot.getChildrenCount();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if (paciente.child("Area").getValue().toString().equals("General")) {
                        general++;
                    } else if (paciente.child("Area").getValue().toString().equals("Dermatología")) {
                        derma++;
                    } else if (paciente.child("Area").getValue().toString().equals("Obstetricia")) {
                        obste++;
                    } else if (paciente.child("Area").getValue().toString().equals("Ortopedia")) {
                        orto++;
                    } else if (paciente.child("Area").getValue().toString().equals("Pediatria")) {
                        pediatria++;
                    } else if (paciente.child("Area").getValue().toString().equals("Psiquiatria")) {
                        psiquatria++;
                    }
                }
                int porgeneral = general * 100 / total;
                int porderma = derma * 100 / total;
                int porpediatria = pediatria * 100 / total;
                int pororto = orto * 100 / total;
                int porpsiquiatria = psiquatria * 100 / total;
                int porobs = obste * 100 / total;

                yVals.add(new BarEntry(0, porgeneral));
                yVals.add(new BarEntry(1, porderma));
                yVals.add(new BarEntry(2, porobs));
                yVals.add(new BarEntry(3, pororto));
                yVals.add(new BarEntry(4, porpediatria));
                yVals.add(new BarEntry(5, porpsiquiatria));
                BarDataSet setEspecialidad = new BarDataSet(yVals, "Data set Tiempo edad");
                setEspecialidad.setColors(ColorTemplate.JOYFUL_COLORS);

                BarData dataEspecialidad = new BarData(setEspecialidad);
                dataEspecialidad.setValueTextSize(10f);
                dataEspecialidad.setValueTextColor(R.color.colorOscuro);

                barEspecialidad.setData(dataEspecialidad);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setProporcion() {
        Query queryProporcion = FirebaseDatabase.getInstance().getReference().child("pacientes");
        queryProporcion.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int total = (int) dataSnapshot.getChildrenCount();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if (paciente.child("Estado").getValue().toString().equals("1")) {
                        entrada++;
                    } else if (paciente.child("Estado").getValue().toString().equals("0")) {
                        salida++;
                    }
                    i++;
                }
                double prop = salida!=0?(entrada / salida):0.0;
                proporcion.setText(prop + "");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setSexoTriage() {
        Description descbar = new Description();
        descbar.setText("Genero por nivel de triage");
        descbar.setTextSize(12);
        descbar.setTextColor(R.color.colorOscuro);

        barSexoTriage.setDescription(descbar);
        barSexoTriage.setFitBars(true);
        barSexoTriage.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barSexoTriage.getAxisRight().setEnabled(false);

        final ArrayList<String> xLabelSexo = new ArrayList<>();
        xLabelSexo.add("Nivel 1");
        xLabelSexo.add("Nivel 1");
        xLabelSexo.add("Nivel 2");
        xLabelSexo.add("Nivel 2");
        xLabelSexo.add("Nivel 3");
        xLabelSexo.add("Nivel 3");

        barSexoTriage.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelSexo.get((int) value);
            }
        });

        Query querytriages = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querytriages.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if (paciente.child("Triage").getValue().toString().equals("1")) {
                        if (paciente.child("Sexo").getValue().toString().equals("M")) {
                            triageunoh++;
                        } else if (paciente.child("Sexo").getValue().toString().equals("F")) {
                            triageunom++;
                        }
                    } else if (paciente.child("Triage").getValue().toString().equals("2")) {
                        if (paciente.child("Sexo").getValue().toString().equals("M")) {
                            triagedosh++;
                        } else if (paciente.child("Sexo").getValue().toString().equals("F")) {
                            triagedosm++;
                        }
                    } else if (paciente.child("Triage").getValue().toString().equals("3")) {
                        if (paciente.child("Sexo").getValue().toString().equals("M")) {
                            triagetresh++;
                        } else if (paciente.child("Sexo").getValue().toString().equals("F")) {
                            triagetresm++;
                        }
                    }
                    i++;
                }
                valoresbarSexoTriage.add(new BarEntry(0, triageunoh));
                valoresbarSexoTriage.add(new BarEntry(1, triageunom));
                valoresbarSexoTriage.add(new BarEntry(2, triagedosh));
                valoresbarSexoTriage.add(new BarEntry(3, triagedosm));
                valoresbarSexoTriage.add(new BarEntry(4, triagetresh));
                valoresbarSexoTriage.add(new BarEntry(5, triagetresm));

                BarDataSet datasetsexo = new BarDataSet(valoresbarSexoTriage, "Triage por sexo");
                datasetsexo.setColors(ColorTemplate.JOYFUL_COLORS);
                datasetsexo.setDrawValues(true);

                BarData datasexo = new BarData(datasetsexo);
                datasexo.setBarWidth(0.5f);

                barSexoTriage.setData(datasexo);
                barSexoTriage.invalidate();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setSangre() {

        ArrayList<Integer> sangres = new ArrayList();
        ArrayList<String> sangress = new ArrayList();
        Query querytriages = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querytriages.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {
                    if(paciente.child("Sangre").getValue().toString().equals("A+"))
                    {
                        amas++;
                    }
                    else if(paciente.child("Sangre").getValue().toString().equals("B+"))
                    {
                        bmas++;
                    }
                    else if(paciente.child("Sangre").getValue().toString().equals("O+"))
                    {
                        omas++;
                    }
                    else if(paciente.child("Sangre").getValue().toString().equals("AB+"))
                    {
                        abmas++;
                    }
                    else if(paciente.child("Sangre").getValue().toString().equals("A-"))
                    {
                        amenos++;
                    }
                    else if(paciente.child("Sangre").getValue().toString().equals("B-"))
                    {
                        bmenos++;
                    }
                    else if(paciente.child("Sangre").getValue().toString().equals("O-"))
                    {
                        omenos++;
                    }
                    else if(paciente.child("Sangre").getValue().toString().equals("AB-"))
                    {
                        abmenos++;
                    }
                }
                sangres.add(amas);
                sangres.add(bmas);
                sangres.add(abmas);
                sangres.add(omas);
                sangres.add(amenos);
                sangres.add(bmenos);
                sangres.add(omenos);
                sangres.add(abmenos);

                sangress.add("A+");
                sangress.add("B+");
                sangress.add("AB+");
                sangress.add("O+");
                sangress.add("A-");
                sangress.add("B-");
                sangress.add("O-");
                sangress.add("AB-");

                int max = Integer.MIN_VALUE;
                int indice = 0;
                for(int j=0; j< sangres.size(); j++)
                {
                    if(sangres.get(j) > max){
                        max = sangres.get(j);
                        indice = j;
                    }
                }
                sangre.setText(sangress.get(indice).toString());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setTiempoTriage(int count, int range)
    {
        ArrayList<BarEntry> yVals = new ArrayList<>();

        Description desc = new Description();
        desc.setText("Prom. tiempo por edades");
        desc.setTextSize(12);
        desc.setTextColor(R.color.colorOscuro);
        barTiempoTriage.setDescription(desc);

        final ArrayList<String> xLabelEdades = new ArrayList<>();
        xLabelEdades.add("Nivel 1");
        xLabelEdades.add("Nivel 2");
        xLabelEdades.add("Nivel 3");

        barTiempoTriage.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabelEdades.get((int) value);
            }
        });
        barTiempoTriage.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barTiempoTriage.getAxisRight().setEnabled(false);

        Query querycumpleaños = FirebaseDatabase.getInstance().getReference().child("pacientes");
        querycumpleaños.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> pacientesChild = dataSnapshot.getChildren();
                totalpacientes = (int) dataSnapshot.getChildrenCount();
                int i = 0;
                for (DataSnapshot paciente : pacientesChild) {

                    if(paciente.child("Triage").getValue().toString().equals("1"))
                    {
                        sumaniveluno = Integer.parseInt(paciente.child("Tiempo").getValue().toString());
                        conuno++;
                    }
                    else if(paciente.child("Triage").getValue().toString().equals("2"))
                    {
                        sumaniveldos = Integer.parseInt(paciente.child("Tiempo").getValue().toString());
                        condos++;
                    }
                    else if(paciente.child("Triage").getValue().toString().equals("3"))
                    {
                        sumaniveltres = Integer.parseInt(paciente.child("Tiempo").getValue().toString());
                        contres++;
                    }
                    i++;
                }
                int promedio1 =0;
                int promedio2 =0;
                int promedio3 = 0;
                if(conuno != 0) {
                    promedio1 = sumaniveluno / conuno;
                }
                if (condos != 0)
                {
                    promedio2 = sumaniveldos/condos;
                }
                if (contres != 0)
                {
                    promedio3 = sumaniveltres/contres;
                }
                yVals.add(new BarEntry(0, promedio1));
                yVals.add(new BarEntry(1, promedio2));
                yVals.add(new BarEntry(2, promedio3));

                BarDataSet setTiempoEdad = new BarDataSet(yVals, "Tiempo de lectura por triage");
                setTiempoEdad.setColors(ColorTemplate.JOYFUL_COLORS);

                BarData dataTiempoEdad = new BarData(setTiempoEdad);
                dataTiempoEdad.setValueTextSize(10f);
                dataTiempoEdad.setValueTextColor(R.color.colorOscuro);

                barTiempoTriage.setData(dataTiempoEdad);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
