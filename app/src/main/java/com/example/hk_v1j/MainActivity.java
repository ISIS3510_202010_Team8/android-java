package com.example.hk_v1j;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hk_v1j.databinding.ActivityMainBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.*;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    //Firebase
    private FirebaseAuth mAuth;

    //Interfaz
    private EditText mEditTextUser;
    private EditText mEditTextContra;
    private CheckBox checkBoxPaciente;
    private Button btn;

    //Base de datos local
    private DatabaseHelper myDb;
    //Varios
    String[] credenciales;
    String pacientelogeado;
    public static boolean logeado;

    ActivityMainBinding binding;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        logeado = false;
        pacientelogeado = "";

        if(mAuth.getCurrentUser() != null)
        {
            updateUI(true);
        }
        else if(logeado == true)
        {
            launchUsuario(pacientelogeado);
        }
        else
        {
            ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
            mEditTextUser = binding.editUser;
            mEditTextContra= binding.editContra;
            checkBoxPaciente = findViewById(R.id.checkBox);
            btn= findViewById(R.id.button_main);
            myDb = new DatabaseHelper(this);
            credenciales= new String[2];
            checkConect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //mAuth.signOut();
    }

    /**
     * Revisa si es paciente o no
     * @param view
     */
    public void launchIngreso(View view)
    {
        if(!checkBoxPaciente.isChecked()) {
            String usuario = mEditTextUser.getText().toString();
            String contrasena = mEditTextContra.getText().toString();
            signInWithEmailAndPassword(usuario, contrasena);
        }
        else {
            String paciente = mEditTextUser.getText().toString();
            String contrasenapaciente = mEditTextContra.getText().toString();
            if(paciente.matches("\\d+(?:\\.\\d+)?")) {
                signInWithCedulaAndPassword(paciente, contrasenapaciente);
            }
        }
    }

    /**
     * Ingresa como enfermero/a
     * @param email
     * @param password
     */
    public void signInWithEmailAndPassword (String email, String password) {
        if(checkConect() && email!=null && password != null){
            try{
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful())
                                {
                                    //GUARDAR LOCAL
                                    try {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                myDb.insertUser(email,password);
                                            }
                                        }).start();

                                    }catch (Exception e){
                                    }
                                    credenciales[0]=email;
                                    credenciales[1]=password;
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    updateUI(true);
                                }
                                else {
                                    updateUI(false);
                                }
                                // ...
                            }
                        });
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        else{
            String rta= myDb.checkUser(email,password);
            Log.d("OFLOGIN",rta);
            if(rta.equals("404")){
                updateUI(false);
            }else{
                updateUI(true);
            }
        }

    }

    /**
     * Ingresa como paciente
     * @param paciente
     * @param contrasenapaciente
     */
    public void signInWithCedulaAndPassword (String paciente, String contrasenapaciente)
    {
        if(checkConect())
        {
            Query queryusuario = FirebaseDatabase.getInstance().getReference().child("pacientes").child(paciente);
            queryusuario.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists())
                    {
                        String num1 = contrasenapaciente.charAt(contrasenapaciente.length() - 3) + "";
                        String num2 = contrasenapaciente.charAt(contrasenapaciente.length() - 2) + "";
                        String num3 = contrasenapaciente.charAt(contrasenapaciente.length() - 1) + "";

                        if(contrasenapaciente.equals("HK"+num1+num2+num3))
                        {
                            pacientelogeado = paciente;
                            logeado = true;
                            launchUsuario(paciente);

                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    /**
     * Ingresa como paciente
     * @param msg
     */
    public void launchUsuario(String msg)
    {
        Intent intent = new Intent(this, Usuario.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        SharedPreferences.Editor epepito = getSharedPreferences("usuario", 0).edit();
        epepito.putString("key_cedula", msg);
        epepito.commit();

        startActivity(intent);
    }

    /**
     * Actualiza el UI
     */
    public void  updateUI(Boolean logeado){

        if(logeado)
        {
            Intent intent = new Intent(this, Home.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else
        {
            alertarUsuario("Error", "Ok", "Cancelar", "Las credenciales que ingresó son incorrectas, por favor verifíquelas e intente nuevamente");
        }
    }

    /**
     * Alertar de un problema a un usuario
     * @param pTitulo
     * @param pPositive
     * @param pNegative
     * @param pMesage
     */

    public void alertarUsuario(String pTitulo, String pPositive, String pNegative, String pMesage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(pTitulo);
        dialog.setMessage(pMesage);
        dialog.setCancelable(true);
        dialog.setPositiveButton(
                pPositive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                    }
                });

        dialog.setNegativeButton(
                pNegative,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });

        AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
    }

    /**
     * Verifica si hay conexión
     * @return
     */
    protected boolean checkConect(){
        ConnectivityManager cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean wifi = false;
        boolean dataP= false;
        for(Network n:cm.getAllNetworks()){
            NetworkInfo ni=cm.getNetworkInfo(n);
            if(ni.getType()==ConnectivityManager.TYPE_WIFI){
                wifi= ni.isConnected();
            }else if(ni.getType()== ConnectivityManager.TYPE_MOBILE){
                dataP=ni.isConnected();
            }
        }
        boolean conected = wifi||dataP;
        if(!conected){
            Toast.makeText(MainActivity.this,"No se puede acceder a internet,\n verifique su conexión",Toast.LENGTH_LONG).show();
            //btn.setEnabled(false);
        }
        return conected;
    }

}
