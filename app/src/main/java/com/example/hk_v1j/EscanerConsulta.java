package com.example.hk_v1j;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.hk_v1j.databinding.ActivityConsultarCedulaBinding;
import com.example.hk_v1j.databinding.ActivityEscanerConsultaBinding;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class EscanerConsulta extends AppCompatActivity {

    SurfaceView cameraView;
    TextView textView;
    CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;
    SharedPreferences sharedPreferences;
    String cedula = "";
    String limpia = "";
    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("consulta", 0);

        ActivityEscanerConsultaBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_escaner_consulta);
        setTitle("Escaner");

        cameraView = binding.surfaceView;
        textView = binding.escaneando;

        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        if (!textRecognizer.isOperational()) {
            Log.w("MainActivity", "Detector dependencies are not yet available");

        } else {
            cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(2.0f)
                    .build();
            cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {
                        while (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(EscanerConsulta.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    RequestCameraPermissionID);
                        }
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });

            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {

                }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if (items.size() != 0) {
                        textView.post(new Runnable() {
                            @Override
                            public void run() {
                                StringBuilder stringBuilder = new StringBuilder();
                                for (int i = 0; i < items.size(); i++) {
                                    TextBlock item = items.valueAt(i);
                                    stringBuilder.append(item.getValue());
                                    stringBuilder.append("\n");


                                }
                                if (stringBuilder.toString().contains("NUMERO")) {
                                    int i = stringBuilder.toString().indexOf("NUMERO");
                                    int c = i + 6;

                                    String elemento = stringBuilder.charAt(c) + "";

                                    StringBuilder cedulaBuilder = new StringBuilder();

                                    while (elemento.contains("0") || elemento.contains("1") || elemento.contains("2") || elemento.contains("3") || elemento.contains("4")
                                            || elemento.contains("5") || elemento.contains("6") || elemento.contains("7") || elemento.contains("8") || elemento.contains("9") ||
                                            elemento.contains(".") || elemento.contains(" ")) {
                                        cedulaBuilder.append(elemento);
                                        c = c + 1;
                                        elemento = stringBuilder.charAt(c) + "";
                                    }
                                    cedula = cedulaBuilder.toString();
                                    textView.setText("NUMERO DE CEDULA: " + cedula);
                                }
                            }
                        });
                    }
                }
            });
        }
    }
    public void validarCedula(View view) {
        if(isOnline())
        {
            String[] cedula2 = cedula.split("\\.");

            for (int p = 0; p < cedula2.length; p++) {
                limpia += cedula2[p];
            }
            limpia = limpia.trim();

            DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("pacientes");
            db.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (!snapshot.hasChild(limpia))
                    {
                        alertarUsuario("Cédula no encontrada", "Ok", "Cancelar", "La cédula no fue encontrada. Por favor revise el número o cree un nuevo formulario.");
                    }
                    else{
                        long elapsedTime = System.nanoTime() - ConsultarCedula.start;
                        long miliseg = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS);
                        Home.recalcularTiemposBuscar(miliseg);
                        launchFormEscaner();
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            Home.numeroConsultas++;
        }
        else
        {
            alertarUsuario("Sin conexión", "Ok", "Cancelar", "No fue posible establecer conexión a la base de datos para consultar la cédula. Por favor cree un nuevo formulario.");
        }
    }

    public void launchFormEscaner() {
        String msg = textView.getText().toString();

        Log.d("AHHHHHH", msg);

        if(!msg.equals("Estamos escaneando la cédula...") && !msg.trim().equals("NUMERO DE CEDULA:")) {
            Intent intent = new Intent(this, FormConsulta.class);
            SharedPreferences.Editor editor = getSharedPreferences("consulta", 0).edit();
            editor.putBoolean("estado_cosulta",true);
            editor.putString("val_cedula","NUMERO:" + cedula);
            editor.commit();

            startActivity(intent);
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void alertarUsuario(String pTitulo, String pPositive, String pNegative, String pMesage)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(pTitulo);
        dialog.setMessage(pMesage);
        dialog.setCancelable(true);

        dialog.setPositiveButton(
                pPositive,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                    }
                });

        dialog.setNegativeButton(
                pNegative,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });
        AlertDialog alert = dialog.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRojoOscuro));
    }
}
