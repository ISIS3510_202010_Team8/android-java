package com.example.hk_v1j;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.example.hk_v1j.databinding.ActivityCheckCamaraBinding;

public class CheckCamara extends AppCompatActivity {
    TextView cam1, cam2;
    ProgressBar pg1, pg2;
    Button btn1, btnPermisos, btnVerificar;
    boolean correcto, valido, verified;
    final static int PERMISSION_READY = 1004;
    ActivityCheckCamaraBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_camara);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_check_camara);

        btnPermisos = binding.verif;
        if (valido) {
            try {
                btnPermisos.setVisibility(View.INVISIBLE);
                btnPermisos.setClickable(false);
                btnVerificar=binding.funcionalidad;
                btnVerificar.setVisibility(View.VISIBLE);
                btnVerificar.setClickable(true);
                cam1 = binding.camChck;
                cam2 = binding.camChck2;
                btnVerificar.setVisibility(View.VISIBLE);
                btnVerificar.setClickable(true);
                btnPermisos.setVisibility(View.INVISIBLE);
                btnVerificar.setEnabled(true);
            } catch ( Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (valido&&correcto) {
            try {
                btnPermisos.setVisibility(View.INVISIBLE);
                btnPermisos.setClickable(false);

                btnVerificar.setVisibility(View.VISIBLE);
                btnVerificar.setClickable(true);
                cam1 = binding.camChck;
                cam2 = binding.camChck2;
                btnVerificar.setVisibility(View.VISIBLE);
                btnVerificar.setClickable(true);
                btnPermisos.setVisibility(View.INVISIBLE);
                btnVerificar.setEnabled(true);
            } catch ( Exception e) {
                e.printStackTrace();
            }
        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d("PERMISO: ", requestCode + "");


        switch (requestCode) {
            case PERMISSION_READY: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnPermisos.setVisibility(View.INVISIBLE);
                    btnPermisos.setClickable(false);
                    btnVerificar=binding.funcionalidad;
                    btnVerificar.setVisibility(View.VISIBLE);
                    btnVerificar.setClickable(true);
                    cam1 = binding.camChck;
                    cam2 = binding.camChck2;
                    btnVerificar.setVisibility(View.VISIBLE);
                    btnVerificar.setClickable(true);
                    btnPermisos.setVisibility(View.INVISIBLE);
                    btnVerificar.setEnabled(true);
                } else {

                }
                return;
            }

        }
    }

    public void getVerif(View view) {
        if (this.checkCameraFunc(this) == true && this.checkCameraHardware(this) == true) {
            pg1 = binding.pg1;
            pg2 = binding.pg2;
            pg1.setProgress(pg1.getMax());
            pg2.setProgress(pg2.getMax());
            if (!verified) {
                cam1.setText(cam1.getText() + ": Cámara encontrada");
                cam2.setText(cam2.getText() + ": Cámara funcional");
                pg1.setVisibility(View.INVISIBLE);
                pg2.setVisibility(View.INVISIBLE);
                verified = true;
                correcto=true;
                btn1 = binding.end;
                btn1.setVisibility(View.VISIBLE);
                btn1.setClickable(true);
                btnVerificar.setVisibility(View.INVISIBLE);
                btnVerificar.setClickable(false);
            }

        } else if (this.checkCameraHardware(this) == false) {
            if (!verified) {
                cam1.setText(cam1.getText() + ": Cámara no disponible");
                cam2.setText(cam2.getText() + ": No es posible verificar funcionalidad");
                verified = true;
            }
        } else {
            if (!verified) {
                cam1.setText(cam1.getText() + ": Cámara funcional");
                cam2.setText(cam2.getText() + ": La cámara no funciona");
                verified = true;
            }
        }

    }

    public void getCheck(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_READY);

        } else {

            //chamera is ok
            correcto = true;
            btnPermisos.setVisibility(View.INVISIBLE);
            btnPermisos.setClickable(false);
            btnVerificar=binding.funcionalidad;
            btnVerificar.setVisibility(View.VISIBLE);
            btnVerificar.setClickable(true);
            cam1 = binding.camChck;
            cam2 = binding.camChck2;
            btnVerificar.setVisibility(View.VISIBLE);
            btnVerificar.setClickable(true);
            btnPermisos.setVisibility(View.INVISIBLE);
            btnVerificar.setEnabled(true);

        }


    }

    public boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public boolean checkCameraFunc(Context context) {
        try {
            android.hardware.Camera mCameraDevice = android.hardware.Camera.open();
            return true;
        } catch (Exception e) {
            //is not working
            return false;
        }
    }

    public void launchFormEscanar(View view) {
        if (correcto) {
            Log.d("PASANDO: ","escaner");
            boolean msg = btn1.getVisibility() == View.VISIBLE;
            Intent intent = new Intent(this, EscanerCrear.class);
            intent.putExtra("chequeo", msg);
            startActivity(intent);
        } else {
            Log.d("PASANDO: ","formulario");
            boolean msg = btn1.getVisibility() == View.VISIBLE;
            Intent intent = new Intent(this, Formulario.class);
            intent.putExtra("chequeo", msg);
            startActivity(intent);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

    }
}
